package api

import (
	"bitbucket.org/cocoon/trainmvp/watcher"
	"context"
	"log"
	"testing"
	"time"

	"bitbucket.org/cocoon/trainmvp/collector"
	"bitbucket.org/cocoon/trainmvp/documentor"

	"github.com/nats-io/nats.go"
)

func injectFakeDocumentation(nc *nats.Conn) (err error) {

	cli := documentor.NewClient(nc)

	e1 := documentor.Entry{"0.1", "watcher check documentation"}
	err = cli.AddEntry("watcher1", "check1", e1)
	err = cli.AddEntry("watcher1", "check2", e1)
	err = cli.AddEntry("watcher2", "check1", e1)
	return err
}
func injectFakeWatcherStatus(nc *nats.Conn) (err error) {
	cli := collector.NewClient(nc)

	err = cli.PublishWatcherStatus("watcher1", "check1", 1)
	err = cli.PublishWatcherStatus("watcher1", "check2", -1)
	err = cli.PublishWatcherStatus("watcher2", "check1", 0)
	return
}

func TestAPI(t *testing.T) {

	ctx, cancel := context.WithCancel(context.Background())

	a := NewAPI(ctx, "nats://127.0.0.1:4222")

	nc, err := a.NatsConnection()
	if err != nil {
		t.Fail()
		return
	}

	// send a fake message to watcher updater
	enc, _ := nats.NewEncodedConn(nc, nats.JSON_ENCODER)
	defer enc.Close()
	m := watcher.WatcherMessage{Result: -1}
	enc.Publish("change.watcher.watcherId.checkId", m)

	// wait message at api.WsChannel

	//
	// documentor
	//
	err = a.StartDocumentor()
	if err != nil {
		t.Fail()
		return
	}
	time.Sleep(1 * time.Second)

	// Add fake documentation entry
	err = injectFakeDocumentation(nc)
	time.Sleep(1 * time.Second)

	data, err := a.GetWatcherDocumentation()
	log.Printf("%s", data)
	if len(data) < 2 {
		t.Fail()
		return
	}

	//
	// Collector
	//
	err = a.StartCollector()
	if err != nil {
		t.Fail()
		return
	}
	time.Sleep(1 * time.Second)

	// publish fake watcher status
	err = injectFakeWatcherStatus(nc)
	time.Sleep(1 * time.Second)

	// fetch watcher status
	stats, err := a.GetWatcherStatus()
	if err != nil {
		t.Fail()
		return
	}
	if len(stats) < 3 {
		t.Fail()
		return
	}
	_ = stats

	scli, err := a.CollectorClient()
	entry, err := scli.GetEntry("watcher2", "check1")
	_ = entry

	//fmt.Printf("%s",stats)

	go func() {
		//  inject watcher status change message
		time.Sleep(60 * time.Second)
		err := enc.Publish("change.watcher.watcher1.check1", watcher.WatcherMessage{Result: 1})
		time.Sleep(2 * time.Second)
		enc.Publish("change.watcher.watcher1.check1", watcher.WatcherMessage{Result: -1})
		time.Sleep(2 * time.Second)
		enc.Publish("change.watcher.watcher1.check1", watcher.WatcherMessage{Result: 0})
		time.Sleep(2 * time.Second)
		enc.Publish("change.watcher.watcher1.check1", watcher.WatcherMessage{Result: -1})

		_ = err
	}()

	time.Sleep(10 * time.Second)

	cancel()
	time.Sleep(1 * time.Second)
	return
}

func DoNothing(api CollectorRequester) {

	doc, err := api.GetWatcherDocumentation()
	_ = err
	_ = doc

	stat, err := api.GetWatcherStatus()
	_ = err
	_ = stat
}

func TestCollectorRequestAPI(t *testing.T) {

	ctx, cancel := context.WithCancel(context.Background())

	a := NewAPI(ctx, "nats://127.0.0.1:4222")

	DoNothing(a)
	_ = cancel
}
