package api

import (
	"log"

	"bitbucket.org/cocoon/trainmvp/collector"

	//"bitbucket.org/cocoon/trainmvp/watcher"
	"context"

	//"bitbucket.org/cocoon/trainmvp/collector"
	"bitbucket.org/cocoon/trainmvp/documentor"

	"github.com/nats-io/nats.go"
)

var (
	Version = "0.2"
)

type WatcherDocumentation documentor.WatcherDocumentation

// API  an api for collector related components
type API struct {
	Ctx      context.Context
	NatsURL  string
	natsConn *nats.Conn
	Version  string
	Period   string // collector Period (eg 30s) , wait period to check idle watchers
	//
}

// NewAPI create a new api with an echo server
func NewAPI(ctx context.Context, natsURL string) (api *API) {
	// create the api
	api = &API{
		Ctx:     ctx,
		NatsURL: natsURL, // nats://127.0.0.1:4222
		Version: Version,
		Period:  "120s",
	}
	return api
}

// return api version
func (api *API) ApiVersion() string {
	return Version
}

// Start all collector related services: Documentor , Collector
func (api *API) Start() error {
	//
	// documentor
	//
	err := api.StartDocumentor()
	if err != nil {
		return err
	}

	//
	// Collector
	//
	err = api.StartCollector()
	if err != nil {
		return err
	}
	return nil
}

// NatsConnection connect to nats
func (api *API) NatsConnection() (nc *nats.Conn, err error) {

	if api.natsConn == nil {
		// create nats connection
		nc, err := nats.Connect(api.NatsURL)
		if err != nil {
			log.Printf("%s\n", err)
			return nc, err
		}
		api.natsConn = nc
	}
	return api.natsConn, nil
}

// StartDocumentor : Start the Documentor service
func (api *API) StartDocumentor() (err error) {

	nc, err := api.NatsConnection()
	if err != nil {
		return err
	}

	// create and start doumentor
	doc := documentor.NewDocumentor(nc, "Documentation.watchers.>")
	doc.Run(api.Ctx)
	return err
}

func (api *API) DocumentorClient() (cli *documentor.Client, err error) {
	nc, err := api.NatsConnection()
	if err != nil {
		return
	}
	cli = documentor.NewClient(nc)
	return
}

// GetWatcherDocumentation : request service
func (api *API) GetWatcherDocumentation() (document WatcherDocumentation, err error) {
	cli, err := api.DocumentorClient()
	if err != nil {
		return
	}
	doc, err := cli.GetDocumentation()
	return WatcherDocumentation(doc), err
}

// StartCollector : Start the IdleCheck service
func (api *API) StartCollector() (err error) {
	nc, err := api.NatsConnection()
	if err != nil {
		return err
	}
	// create and start collector
	collector.InitCollector(api.Ctx, nc, api.Period)
	return err
}

// CollectorClient return a client to call watcher status
func (api *API) CollectorClient() (cli *collector.Client, err error) {
	nc, err := api.NatsConnection()
	if err != nil {
		return
	}
	cli = collector.NewClient(nc)
	return
}

// GetWatcherStatus : request service
func (api *API) GetWatcherStatus() (entries map[string]collector.TimeSample, err error) {
	cli, err := api.CollectorClient()
	if err != nil {
		return
	}
	entries, err = cli.GetWatcherStates()
	return
}
