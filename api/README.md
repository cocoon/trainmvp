# collector api


an api to regroup all collector related functions

    StartCollector()
    StartDocumentor()

    GetWatcherStatus() (map[string]collector.TimeSample,error)
    GetWatcherDocumentation() (WatcherDocumentation,error)

    CollectorClient()
    DocumentorClient()

    NatsConnection()

a request only collector interface

    GetWatcherStatus() (map[string]collector.TimeSample,error)
    GetWatcherDocumentation() (WatcherDocumentation,error)