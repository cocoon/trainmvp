package api

import (
	"bitbucket.org/cocoon/trainmvp/collector"
)

//  CollectorRequester interface to request api
type CollectorRequester interface {
	GetWatcherDocumentation() (document WatcherDocumentation, err error)
	GetWatcherStatus() (entries map[string]collector.TimeSample, err error)
	CollectorClient() (cli *collector.Client, err error)
}
