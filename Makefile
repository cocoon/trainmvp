-include .env

PROJECTNAME=$(shell basename "$(PWD)")

GOBASE=$(shell pwd)
#GOPATH=$(GOBASE)
GOFILES := $(wildcard *.go)

# PID file will store the server process id when it's running on development mode
NATS_PID=/tmp/.$(PROJECTNAME)-nats-server.pid

# Make is verbose in Linux. Make it silent.
#MAKEFLAGS += --silent

hello :
	echo "Hello ${PROJECTNAME} at ${GOBASE}, GOPATH=${GOPATH}";


start-nats:
	@echo "start nats-server"
	nats-server 2>&1 & echo $$! > $(NATS_PID)
	@cat $(NATS_PID) | sed "/^/s/^/  \>  PID: /"

stop-nats:
	@-touch $(NATS_PID)
	@-kill `cat $(NATS_PID)` 2> /dev/null || true
	@-rm $(NATS_PID)


tests:
	go test bitbucket.org/cocoon/trainmvp

build-images:
	#cd docker/images/collector &&  docker build -t local/collector --force-rm --no-cache .
	cd docker/images/webcollector &&  docker build -t local/collector --force-rm --no-cache .
	#cd docker/images/proxy &&  docker build -t local/proxy --force-rm --no-cache .

build-infra:
	cd docker && docker-compose build --force-rm --no-cache
	#cd docker && docker-compose -f docker-compose-host.yml build --force-rm --no-cache
#build-web:
#	# build web collector
#	rm -rf docker/webcollector/assets
#	cp -rf web/assets docker/webcollector/
#	cd docker/images/webcollector &&  docker build -t local/collector --force-rm --no-cache .
#
build-watchers:
	echo "Compiling fakewatcher for linux"
	GOOS=linux GOARCH=amd64 go build -o build/packages/linux_amd64/fakewatcher ./cmd/fakewatcher/fakewatcher.go
	echo "Compiling fakewatcher for osx"
	GOOS=darwin GOARCH=amd64 go build -o build/packages/darwin_amd64/fakewatcher ./cmd/fakewatcher/fakewatcher.go
	echo "Compiling subscriber for linux"
	GOOS=linux GOARCH=amd64 go build -o build/packages/linux_amd64/subscriber ./cmd/subscriber/subscriber.go
	echo "Compiling subscriber for osx"
	GOOS=darwin GOARCH=amd64 go build -o build/packages/darwin_amd64/subscriber ./cmd/subscriber/subscriber.go
	echo "Compiling fakecomponent for linux"
	GOOS=linux GOARCH=amd64 go build -o build/packages/linux_amd64/fakecomponent ./cmd/fakecomponent/fakecomponent.go
	echo "Compiling fakecomponent for osx"
	GOOS=darwin GOARCH=amd64 go build -o build/packages/darwin_amd64/fakecomponent ./cmd/fakecomponent/fakecomponent.go



