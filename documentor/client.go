package documentor

import (
	"encoding/json"
	"fmt"
	"github.com/nats-io/nats.go"
	"time"
)

type Client struct {
	*nats.Conn
}

func NewClient(nc *nats.Conn) (cli *Client) {
	cli = &Client{nc}
	return cli
}

func (c *Client) GetEntry(watcher, check string) (entry Entry, err error) {

	subject := fmt.Sprintf("get.Documentation.watchers.%s.%s", watcher, check)
	msg, err := c.Request(subject, []byte(""), 1*time.Second)
	if err != nil {
		return
	}
	err = json.Unmarshal(msg.Data, &entry)
	return
}

func (c *Client) GetDocumentation() (doc WatcherDocumentation, err error) {
	msg, err := c.Request("get.Documentation.watchers", []byte(""), 1*time.Second)
	if err != nil {
		return
	}
	err = json.Unmarshal(msg.Data, &doc)
	return

}

func (c *Client) AddEntry(watcher, check string, entry Entry) (err error) {
	subject := fmt.Sprintf("Documentation.watchers.%s.%s", watcher, check)
	data, err := json.Marshal(entry)
	if err != nil {
		return
	}
	err = c.Publish(subject, data)
	return
}
