package documentor

import (
	"context"
	"encoding/json"
	"log"
	"strings"
	"sync"

	"github.com/nats-io/nats.go"
)

var Storage = make(Store)
var StorageLock sync.RWMutex

type Documentor struct {
	*nats.Conn
	subscription string // root for subscribe eg Documentation.watchers.>
	//storage Store

}

func NewDocumentor(nc *nats.Conn, root string) *Documentor {

	return &Documentor{nc, root}
}

func (d *Documentor) Run(ctx context.Context) {

	// start subscribing to documentation message
	s1, err := d.Subscribe(d.subscription, d.Receive)
	if err != nil {
		log.Printf("Documentor: %s\n", err.Error())
		return
	}
	//defer s1.Unsubscribe()
	log.Printf("Documentor: Receiver start listening at %s\n", d.subscription)

	// start get entry service : get.Documentation.watchers
	// get.Documentation.watchers.*.*
	s2, err := d.Subscribe("get.Documentation.watchers.>", d.GetService)
	if err != nil {
		log.Printf("Documentor: %s\n", err.Error())
		return
	}
	// start get allservice : get.Documentation.watchers
	// get.Documentation.watchers.*.*
	s3, err := d.Subscribe("get.Documentation.watchers", d.GetService)
	if err != nil {
		log.Printf("Documentor: %s\n", err.Error())
		return
	}

	//defer s2.Unsubscribe()
	log.Printf("Documentor: service start listening at get.Documentation.watchers.>\n")

	go func() {

		select {
		case <-ctx.Done():
			log.Printf("Documentor: exiting\n")
			err = s1.Unsubscribe()
			err = s2.Unsubscribe()
			err = s3.Unsubscribe()
			_ = err
			return
		}
	}()

}

func (d *Documentor) Receive(msg *nats.Msg) {

	log.Printf("Documentor.Receive: receive message on: %s\n", msg.Subject)
	subject, err := NewSubject(msg.Subject)
	if err != nil {
		log.Printf("Documentor.Receive: bad subject :%s\n", msg.Subject)
		return
	}
	var e Entry
	err = json.Unmarshal(msg.Data, &e)
	if err != nil {
		log.Printf("Documentor.Receive : %s\n", err.Error())
	}
	Add(subject.Item, subject.Record, e)

}

// service functions

type GetEntryIn struct {
	Watcher string
	Check   string
}

// GetService get.Documentation.watchers  { watcher , check } -> Entry
func (d *Documentor) GetAllService(msg *nats.Msg) {

	log.Printf("Documentor.GetAllservice: receive request: %s\n", msg.Subject)

	// subject get.Documentation.watchers.>
	subject := strings.Split(msg.Subject, ".")

	if len(subject) == 3 {
		//  request all   get.Documentation.watchers
		data, err := json.Marshal(Storage)
		if err != nil {
			log.Printf("Documentor.GetAllservice: Storage error \n")
			return
		}
		d.Publish(msg.Reply, data)
		return
	}
}

// GetService get.Documentation.watchers  { watcher , check } -> Entry
func (d *Documentor) GetService(msg *nats.Msg) {

	log.Printf("Documentor.Getservice: receive request: %s\n", msg.Subject)

	// subject get.Documentation.watchers.>
	subject := strings.Split(msg.Subject, ".")

	if len(subject) == 3 {
		//  request all   get.Documentation.watchers
		data, err := json.Marshal(Storage)
		if err != nil {
			log.Printf("Documentor.Getservice: Storage error \n")
			return
		}
		d.Publish(msg.Reply, data)
		return
	}

	if len(subject) < 5 {
		log.Printf("Documentor.Getservice: bad subject :%s\n", msg.Subject)
		return
	}

	// subject get.Documentation.watchers.watcherId.checkId
	watcher := subject[3]
	check := subject[4]

	var entry Entry
	entry = Get(watcher, check)

	data, err := json.Marshal(entry)
	if err != nil {
		log.Printf("Documentor.GetService : %s\n", err.Error())
		d.Publish(msg.Reply, []byte("error"))
	} else {
		d.Publish(msg.Reply, data)
	}
}

// storage functions

func Add(item, record string, entry Entry) {
	StorageLock.Lock()
	defer StorageLock.Unlock()
	_, ok := Storage[item]
	if ok == false {
		Storage[item] = Item{}
	}
	Storage[item][record] = entry
}

func Get(item, record string) (entry Entry) {
	//entry = Entry{}
	StorageLock.RLock()
	defer StorageLock.RUnlock()
	entry = Storage[item][record]
	return entry
}

func GetAll() (data []byte, err error) {
	StorageLock.RLock()
	defer StorageLock.RUnlock()
	data, err = json.Marshal(Storage)
	return data, err
}
