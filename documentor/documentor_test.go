package documentor

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"testing"
	"time"

	"github.com/nats-io/nats.go"
)

func TestDocumentor_Storage(t *testing.T) {

	e1 := Entry{"0.1", "watcher1 check1 documentation"}
	//d1, _ := json.Marshal(e1)
	e2 := Entry{"0.1", "watcher1 check2 documentation"}
	//d2, _ := json.Marshal(e2)

	Add("watcher1", "check1", e1)
	Add("watcher1", "check2", e2)

	x1 := Get("watcher1", "check1")
	x2 := Get("watcher1", "check2")
	x := Get("watcher1", "dummy")
	xx := Get("dummy", "dummy")

	_ = x1
	_ = x2
	_ = x
	_ = xx

	data, err := json.Marshal(Storage)
	if err != nil {
		t.Fail()
		return
	}
	fmt.Printf("%s\n", data)

}

func TestDocumentor_Run(t *testing.T) {

	e1 := Entry{"0.1", "watcher1 check1 documentation"}
	d1, _ := json.Marshal(e1)
	e2 := Entry{"0.1", "watcher1 check2 documentation"}
	d2, _ := json.Marshal(e2)

	// Connect to a server
	nc, err := nats.Connect("127.0.0.1:4222")
	if err != nil {
		fmt.Printf("%s\n", err)
		t.Fail()
		return
	}

	ctx, cancel := context.WithCancel(context.Background())

	doc := NewDocumentor(nc, "Documentation.watchers.>")
	doc.Run(ctx)

	// send message to fill storage
	err = nc.Publish("Documentation.watchers.watcher1.check1", d1)
	err = nc.Publish("Documentation.watchers.watcher1.check2", d2)
	time.Sleep(1 * time.Second)
	// queries
	msg, err := nc.Request("get.Documentation.watchers.watcher1.check1", []byte(""), 1*time.Second)
	_ = msg
	if err != nil {
		log.Printf("%s\n", err.Error())
		t.Fail()
		return
	} else {
		log.Printf("reply: %s", msg.Data)
	}

	msg, err = nc.Request("get.Documentation.watchers", []byte(""), 1*time.Second)
	if err != nil {
		log.Printf("%s\n", err.Error())
		t.Fail()
		return
	} else {
		log.Printf("reply: %s", msg.Data)
	}

	time.Sleep(3 * time.Second)

	cancel()
	time.Sleep(2 * time.Second)
}

func TestDocumentor_Client(t *testing.T) {

	e1 := Entry{"0.1", "watcher1 check1 documentation"}
	e2 := Entry{"0.1", "watcher1 check2 documentation"}

	// Connect to a server
	nc, err := nats.Connect("127.0.0.1:4222")
	if err != nil {
		fmt.Printf("%s\n", err)
		t.Fail()
		return
	}

	// start service
	ctx, cancel := context.WithCancel(context.Background())
	doc := NewDocumentor(nc, "Documentation.watchers.>")
	doc.Run(ctx)
	time.Sleep(1 * time.Second)

	// create client
	cli := NewClient(nc)

	// send data
	err = cli.AddEntry("watcher1", "check1", e1)
	err = cli.AddEntry("watcher1", "check2", e2)
	if err != nil {
		t.Fail()
		return
	}
	time.Sleep(1 * time.Second)

	// get entry
	entry, err := cli.GetEntry("watcher1", "check1")
	if err != nil {
		t.Fail()
		return
	}
	if entry.Version != "0.1" {
		t.Fail()
		return
	}

	// get all
	documentation, err := cli.GetDocumentation()
	if err != nil {
		t.Fail()
		return
	}
	if documentation["watcher1"]["check1"].Version != "0.1" {
		t.Fail()
		return
	}

	cancel()
	time.Sleep(1 * time.Second)

}
