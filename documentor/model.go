package documentor

import (
	"errors"
	"strings"
)

// struct of a source subject eg Documentation
// eg Documentation.watchers.<watcherId>.<checkId>
// or Documentation.<collection>.<Item>.<Record>
type Subject struct {
	Prefix      string // eg Documentation
	Collections string // eg watchers
	Item        string // eg watcherId
	Record      string // eg checkId
	Wild        string // rest of the subject
}

func NewSubject(subject string) (d *Subject, err error) {
	parts := strings.Split(subject, ".")
	if len(parts) < 4 {
		err = errors.New("bad subject")
		return
	}
	d = &Subject{parts[0], parts[1], parts[2], parts[3], ""}

	if len(parts) >= 5 {
		d.Wild = strings.Join(parts[4:len(parts)-1], ".")
	}
	return
}

// structure of a record ( a check )
type Entry struct {
	Version string
	Text    string
}

//  WatcherDocumentation : structure for importing dict
type WatcherDocumentation map[string]map[string]Entry

type Item map[string]Entry

/*
"title" : {
	"Version" : "0.1",
	"Text": "watcher idm api",
},

*/

type Store map[string]Item
