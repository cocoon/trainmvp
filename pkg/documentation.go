package trainmvp

import (
	nats "github.com/nats-io/nats.go"
)

// DocumentationMessage : documetation message structure
type DocumentationMessage struct {
	Version string
	Text    string
}

// DocumentationPublisher function to publish doc
type DocumentationPublisher func(check string, body DocumentationMessage)

// DocumentationPublisherFactory returns a DocumentationPublisher function
// prefix : eg Documenatation.watcher.watcherID
func DocumentationPublisherFactory(nc *nats.Conn, prefix string) (publisher DocumentationPublisher) {

	ec, _ := nats.NewEncodedConn(nc, nats.JSON_ENCODER)

	publisher = func(check string, message DocumentationMessage) {
		subject := prefix + "." + check
		ec.Publish(subject, message)
	}
	return publisher

}
