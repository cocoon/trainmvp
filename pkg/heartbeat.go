package trainmvp

import (
	"context"
	"fmt"
	"log"
	"strconv"
	"time"

	nats "github.com/nats-io/nats.go"
)

/*

	emits periodicaly heartbeat message on nats

	subject:   Heartbeat.<category>.<name>
	payload:   {"uptime":int}

*/

var RootTopic = "Heartbeat"
var DefaultPeriod int = 5

// Emitter : Heartbeat emiter  on subject Heartbeat.Name
type Heartbeat struct {
	*nats.Conn
	Topic  string        // root topic to identify Heartbeat event ( Heartbeat )
	Name   string        // name of the device : eg sniffer.S1
	Period time.Duration // eg 10 * time.Second

	started time.Time //
}

// Publish : publish mess	age on  sunbject Heartbeat.<Name>
func (p *Heartbeat) Publish(topic string, message []byte) (err error) {

	// compute full topic
	//topic = p.Topic + topic
	subject := fmt.Sprintf("%s.%s", p.Topic, topic)
	// publish to nats
	err = p.Conn.Publish(subject, message)
	return err
}

// CheckExists : check if a heartbeat already exists
func (p *Heartbeat) CheckExists() bool {

	subject := fmt.Sprintf("%s.%s", p.Topic, p.Name)
	wait := p.Period + 1*time.Second

	// wait for a heartbeat
	sub, err := p.SubscribeSync(subject)
	_, err = sub.NextMsg(wait)
	if err != nil {
		// timeout or error : does not exists return false
		fmt.Printf("Heartbeat: NO heartbeat detected for %s\n", subject)
		return false
	}
	// heartbeat already exists : return true
	log.Printf("Heartbeat: heartbeat detected for %s\n", subject)
	return true

}

// Start the timer
func (p *Heartbeat) Start(ctx context.Context) {
	log.Printf("Heartbeat emitter %s started ", p.Name)

	go func() {

		for {
			now := time.Now()
			elapsed := now.Sub(p.started)
			message := fmt.Sprintf(`{"uptime":%d}`, int(elapsed.Seconds()))

			topic := p.Name
			p.Publish(topic, []byte(message))

			// wait for Done()
			select {
			case <-ctx.Done():
				log.Printf("Heartbeat emitter %s stopped\n", p.Name)
				return
			default:
				time.Sleep(p.Period)

			}

		}
	}()

}

// NewEmitter : create a Heartbeat Emitter
func NewHeartbeat(nc *nats.Conn, name string, period int) (*Heartbeat, error) {

	if period == 0 {
		period = DefaultPeriod // send every 5 seconds
	}
	x := strconv.Itoa(period)
	_ = x
	sPeriod, err := time.ParseDuration(strconv.Itoa(period) + "s")
	if err != nil {
		sPeriod, _ = time.ParseDuration("5s")
	}

	p := &Heartbeat{Conn: nc, Name: name, Topic: RootTopic, Period: sPeriod, started: time.Now()}

	p.Conn = nc

	msg := fmt.Sprintf("Heartbeat emitter %s created ", p.Name)
	log.Println(msg)

	return p, err
}
