package trainmvp

import (
	"context"
	"log"
	"testing"
	"time"

	nats "github.com/nats-io/nats.go"
)

func TestHeartbeat(t *testing.T) {

	server := "nats://127.0.0.1:4222"

	nc, err := nats.Connect(server)
	if err != nil {
		log.Printf("Heartbeat emitter: cannot open nats connection: %s", err.Error())
		return
	}

	h, _ := NewHeartbeat(nc, "watcher.watcherID", 1)

	ctx, cancel := context.WithCancel(context.Background())

	h.Start(ctx)

	detected := h.CheckExists()

	if detected == false {
		t.Fail()
		return
	}

	//time.Sleep(10 * time.Second)

	cancel()

	time.Sleep(1 * time.Second)

}
