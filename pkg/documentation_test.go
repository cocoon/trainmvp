package trainmvp

import (
	"log"
	"testing"

	nats "github.com/nats-io/nats.go"
)

func TestDocumentation(t *testing.T) {

	server := "nats://127.0.0.1:4222"

	nc, err := nats.Connect(server)
	if err != nil {
		log.Printf("Documentation emitter: cannot open nats connection: %s", err.Error())
		return
	}

	publisher := DocumentationPublisherFactory(nc, "Documentation.watcher.watcherID")

	message := DocumentationMessage{Version: "0.1", Text: "This is a documentation message"}
	publisher("check1", message)

}
