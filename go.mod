module bitbucket.org/cocoon/trainmvp

go 1.16

require (
	github.com/labstack/echo-contrib v0.9.0
	github.com/labstack/echo/v4 v4.1.17
	github.com/labstack/gommon v0.3.0
	github.com/nats-io/nats-server/v2 v2.1.2
	github.com/nats-io/nats.go v1.10.0
	github.com/pkg/errors v0.9.1
	github.com/prometheus/client_golang v1.9.0
	golang.org/x/net v0.0.0-20200822124328-c89045814202
)
