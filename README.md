# mvp for train identity


## tools for a functional supervision

## components

* watchers
* collector
* prometheus server
* dispatcher a prometheus alertmanager module
* message backbone ( nats )


### watchers 

watchers perform requests to a component ,
analyse results and publish a status OK/KO on the message backbone

eg watcher.<watcherId>.<checkId>   { result: 1 }


### collector

The collector is a prometheus server subscribing to watchers  watcher.>
and build prometheus metrics of the form  watcherUp  id=<watcherId> check=<checkId> $result 


### prometheus

a prometheus instance scrapping the collectors


### dispatcher ( alertmanager )

a prometheus alarm module to handle and dispatch alarms 


### message backbone

a nats server or cluster serving as a core messenger backbone





