package collector

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"testing"
	"time"
)

func TestIdleCheck(t *testing.T) {

	ctx, cancel := context.WithCancel(context.Background())

	//idleTime, _ := time.ParseDuration("5s")

	// create idle checker
	ic, _ := NewStateStore(nil)
	//ic := NewIdleCheck(idleTime)

	// add some item
	ic.Add("watcher.watcherOne.checkOne", 1)
	ic.Add("watcher.tb2.sample.watcherOne.checkTwo", 1)

	s1, _ := ic.Get("watcher.watcherOne.checkOne")
	if s1.Status != 1 {
		t.Fail()
		return
	}
	s2, _ := ic.Get("watcher.tb2.sample.watcherOne.checkTwo")
	if s2.Status != 1 {
		t.Fail()
		return
	}

	keys := ic.GetKeys()
	ok := false
	if len(keys) == 2 {
		if keys[1] == "watcher.watcherOne.checkOne" {
			if keys[0] == "watcher.tb2.sample.watcherOne.checkTwo" {
				// OK
				ok = true
			}
		}
	}
	if ok == false {
		t.Fail()
		return
	}

	m := ic.GetAll()
	_ = m
	j, err := json.Marshal(m)
	if err == nil {
		fmt.Println(string(j))
	}

	ic.scanPeriod = 1 * time.Second
	ic.timeout = 2 * time.Second

	go ic.Run(ctx)

	// wait for storage.timeout
	time.Sleep(6 * time.Second)

	// check value should be 0 with a notification
	ts, ok := ic.Get("watcher.watcherOne.checkOne")
	if ok == true && ts.Status == 0 {
		// should trigger a notification
	} else {
		t.Fail()
		cancel()
		return
	}

	cancel()
	time.Sleep(1 * time.Second)

	log.Print("Done")
	_ = ctx

}
