package collector

/*
	idlecheck

	check when a watcher is idle and emit a metric with value 0
	meaning the watcher is down


*/

import (
	"bitbucket.org/cocoon/trainmvp/watcher"
	"context"
	"encoding/json"
	"log"
	"sort"
	"sync"
	"time"

	"github.com/nats-io/nats.go"
)

// parameters
var ScanPeriod = 10 * time.Second
var IdleValue float64 = 0 // value for reset

var lock = sync.RWMutex{}

//// TimeSample : A status and a timestamp
//type TimeSample struct {
//	Timestamp time.Time `json:"timestamp"`
//	Status    float64   `json:"status"`
//}

type IdleCheck struct {
	IdleTimeout time.Duration         // ttl of a timesample
	store       map[string]TimeSample // eg [watcher.<watcherId>.<checkID]
}

// NewIdleCheck : return a idle checker
func NewIdleCheck(idleTime time.Duration) (idleCheck *IdleCheck) {

	store := make(map[string]TimeSample)
	idleCheck = &IdleCheck{
		idleTime, store,
	}
	return
}

// Add : add a subject to store
func (idle *IdleCheck) Add(subject string, status float64) {
	lock.Lock()
	defer lock.Unlock()
	idle.store[subject] = TimeSample{time.Now(), status}
	//idle.store[subject] = time.Now().Add(idle.IdleTimeout)
}

// Get : get a specific subject time sample
func (idle *IdleCheck) Get(subject string) TimeSample {
	lock.RLock()
	defer lock.RUnlock()
	return idle.store[subject]
}

// GetAll : get all subjects
func (idle *IdleCheck) GetAll() map[string]TimeSample {
	lock.RLock()
	defer lock.RUnlock()
	m := make(map[string]TimeSample)
	for k, v := range idle.store {
		m[k] = v
	}
	return m
}

// GetKeys : keys of the store
func (idle *IdleCheck) GetKeys() (keys []string) {
	lock.RLock()
	defer lock.RUnlock()
	for k := range idle.store {
		keys = append(keys, k)
	}
	// sort keys
	sort.Strings(keys)
	return keys
}

// Run : starts the check loop until ctx.Done
func (idle *IdleCheck) Run(ctx context.Context) {

	go func() {
		for {

			// check Done
			timer := time.NewTimer(ScanPeriod)
			select {
			case <-ctx.Done():
				return
			case <-timer.C:
				//pass
			}

			// delay between 2 checks
			//time.Sleep(ScanPeriod)   // 10s

			// check loop
			//log.Printf("IdleCheck: start scanning subjects")
			now := time.Now()
			lock.Lock()
			for subject, ts := range idle.store {
				if ts.Status == 0 {
					// reset already Done
					continue
				}

				if now.After(ts.Timestamp.Add(idle.IdleTimeout)) {
					// timeout : emit a metric with value 0
					sub := watcher.WatcherSubject{}
					err := sub.Parse(subject)
					//tb := strings.Split(subject, ".")
					if err == nil {
						// split subject
						//prefix := tb[0]
						//itemID := tb[1]
						//checkID := tb[2]

						// reset the timeSample
						idle.store[subject] = TimeSample{now, 0}

						switch sub.Prefix {
						case "watcher":
							// make a watcher_check_status metric with value 0
							WatcherCheckStatus.WithLabelValues(
								sub.Platform, sub.Component, sub.WatcherID, sub.CheckID).Set(IdleValue)
						}
					}
				}
			}
			lock.Unlock()
			//log.Printf("IdleCheck: stop scanning subjects (%s)\n", time.Since(now))

		}
	}()

}

/*
	idlecheck nats service

	get.collector.store.all     -> { map[string]TimeSample }
	get.collector.store.key ( string )  -> TimeSample

*/

type IdleCheckService struct {
	*nats.Conn
	//store *IdleCheck
	store *StateStore
}

func NewIdleCheckService(nc *nats.Conn, store *StateStore) (service *IdleCheckService) {
	service = &IdleCheckService{nc, store}
	return
}

func (s *IdleCheckService) Run(ctx context.Context) {

	go func() {
		s1, err := s.Subscribe("get.collector.store.all", s.GetAll)
		if err != nil {
			log.Printf("idleCheclService: %s\n", err.Error())
			return
		}
		defer s1.Unsubscribe()
		log.Printf("IdleCheckService: start service: get.collector.store.all\n")

		s2, err := s.Subscribe("get.collector.store.key", s.GetKey)
		if err != nil {
			log.Printf("idleCheclService: %s\n", err.Error())
			return
		}
		defer s2.Unsubscribe()
		log.Printf("IdleCheckService: start service: get.collector.store.key\n")

		select {
		case <-ctx.Done():
			log.Printf("IdleCheckService: stop services\n")

			return
		}
	}()
}

func (s *IdleCheckService) GetAll(msg *nats.Msg) {

	m := s.store.GetAll()
	r, _ := json.Marshal(m)
	s.Publish(msg.Reply, r)
}

func (s *IdleCheckService) GetKey(msg *nats.Msg) {

	key := string(msg.Data)
	m, _ := s.store.Get(key)
	r, _ := json.Marshal(m)

	s.Publish(msg.Reply, r)
}
