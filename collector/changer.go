package collector

import (
	"bitbucket.org/cocoon/trainmvp/watcher"
	"fmt"
	"github.com/nats-io/nats.go"
)

/*

	changer.go

	emit change state message for watchers

	eg
	change.watcher.<watcherId>.<checkId>  { "Result":1}

*/

type ChangeHandler struct {
	enc *nats.EncodedConn
}

func NewChangeHandler(nc *nats.Conn) (h *ChangeHandler, err error) {
	enc, err := nats.NewEncodedConn(nc, nats.JSON_ENCODER)
	if err != nil {
		return
	}
	return &ChangeHandler{enc: enc}, nil
}

func (h *ChangeHandler) Send(key string, status int) {
	subject := fmt.Sprintf("change.%s", key) // eg change.watcher.<watcherId>.<checkId>
	msg := watcher.WatcherMessage{Result: status}
	h.enc.Publish(subject, msg)
	return
}
