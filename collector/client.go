package collector

import (
	"bitbucket.org/cocoon/trainmvp/watcher"
	"encoding/json"
	"fmt"
	"time"

	"github.com/nats-io/nats.go"
)

/*
	Idlecheck Client


*/

type Client struct {
	*nats.Conn
}

func NewClient(nc *nats.Conn) (cli *Client) {
	cli = &Client{nc}
	return cli
}

// GetEntry
func (c *Client) GetEntry(watcher, check string) (entry TimeSample, err error) {

	subject := fmt.Sprintf("get.collector.store.key")

	// watcher.watcherOne.checkOne
	key := fmt.Sprintf("watcher.%s.%s", watcher, check)

	msg, err := c.Request(subject, []byte(key), 1*time.Second)
	if err != nil {
		return
	}
	entry = TimeSample{}
	err = json.Unmarshal(msg.Data, &entry)
	return
}

// GetWatcherStates
func (c *Client) GetWatcherStates() (entries map[string]TimeSample, err error) {

	subject := fmt.Sprintf("get.collector.store.all")

	msg, err := c.Request(subject, []byte(""), 1*time.Second)
	if err != nil {
		return
	}
	entries = make(map[string]TimeSample)
	err = json.Unmarshal(msg.Data, &entries)

	return
}

// PublishWatcherStatus publish a status watcher watcher.<watcherId>.<checkId>
func (c *Client) PublishWatcherStatus(watcher, check string, status float64) (err error) {
	subject := fmt.Sprintf("watcher.%s.%s", watcher, check)
	payload := fmt.Sprintf(`{"Result":%d}`, int(status))
	err = c.Publish(subject, []byte(payload))
	return
}

// PublishWatcherStatus2 publish a status watcher
func (c *Client) PublishWatcherStatus2(subject *watcher.WatcherSubject, status float64) (err error) {
	sub := fmt.Sprintf("watcher.%s.%s.%s.%s",
		subject.Platform, subject.Component, subject.WatcherID, subject.CheckID)
	payload := fmt.Sprintf(`{"Result":%d}`, int(status))
	err = c.Publish(sub, []byte(payload))
	return
}
