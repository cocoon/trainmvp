package collector_test

import (
	"bitbucket.org/cocoon/trainmvp/collector"
	"bitbucket.org/cocoon/trainmvp/watcher"
	"context"
	"fmt"
	"github.com/nats-io/nats.go"
	"testing"
	"time"
)

func TestCollector(t *testing.T) {

	addr := ":8080"

	// Connect to a server
	nc, err := nats.Connect("127.0.0.1:4222")
	if err != nil {
		fmt.Printf("%s\n", err)
		t.Fail()
		return
	}

	ctx, cancel := context.WithCancel(context.Background())

	collector.Collect(ctx, nc, addr)

	// send fake messages
	c, _ := nats.NewEncodedConn(nc, nats.JSON_ENCODER)
	defer c.Close()
	m := &watcher.WatcherMessage{Result: 2}
	c.Publish("watcher.sample.check", m)
	c.Publish("component.ComponentID", m)
	c.Publish("watcher.tb2.idm.sample.check", m)

	time.Sleep(60 * time.Second)

	cancel()
	time.Sleep(6 * time.Second)
	fmt.Println("Done")

}
