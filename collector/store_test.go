package collector

import (
	"context"
	"testing"
	"time"
)

func TestStateStore(t *testing.T) {

	var ts TimeSample
	var ok bool

	storage, _ := NewStateStore(nil)

	w1 := "watcher.watcher1.check1"
	storage.Add(w1, 1)
	ts, ok = storage.Get(w1)
	if ok == true && ts.Status == 1 {
		// should trigger a notification
	} else {
		t.Fail()
		return
	}

	storage.Add(w1, 1)
	ts, ok = storage.Get(w1)
	if ok == true && ts.Status == 1 {
		// should NOT trigger a notification
	} else {
		t.Fail()
		return
	}

	storage.Add(w1, -1)
	ts, ok = storage.Get(w1)
	if ok == true && ts.Status == -1 {
		// should trigger a notification
	} else {
		t.Fail()
		return
	}

	all := storage.GetAll()
	_ = all
	if len(all) == 1 {
		if all[w1].Status != -1 {
			t.Fail()
		}
	} else {
		t.Fail()
	}

}

func TestStateStoreRun(t *testing.T) {

	var ts TimeSample
	var ok bool

	ctx, cancel := context.WithCancel(context.Background())

	s, _ := NewStateStore(nil)

	w1 := "watcher.watcher1.check1"
	s.Add(w1, 1)
	ts, ok = s.Get(w1)
	if ok == true && ts.Status == 1 {
		// should trigger a notification
	} else {
		t.Fail()
		cancel()
		return
	}

	s.scanPeriod = 1 * time.Second
	s.timeout = 2 * time.Second

	go s.Run(ctx)

	// wait for storage.timeout
	time.Sleep(6 * time.Second)
	// value should be 0 with a notification
	ts, ok = s.Get(w1)
	if ok == true && ts.Status == 0 {
		// should trigger a notification
	} else {
		t.Fail()
		cancel()
		return
	}

	cancel()

	time.Sleep(1 * time.Second)

}
