package collector

import (
	"context"
	"log"
	"sort"
	"sync"
	"time"

	"bitbucket.org/cocoon/trainmvp/watcher"
	"github.com/nats-io/nats.go"
)

/*

	store is a dictionary map[string]Timesample to store a status with a time stamp

	can send a notify message when a status change
	eg change.watcher.watcherId.checkId  { Result: int }

*/

//var slock = sync.RWMutex{}

// TimeSample : A status and a timestamp
type TimeSample struct {
	Timestamp time.Time `json:"timestamp"`
	Status    float64   `json:"status"` // eg 1/0/-1
}

type StateStore struct {
	lock       *sync.RWMutex
	scanPeriod time.Duration         // period between 2 check
	timeout    time.Duration         // timeout for idlecheck
	store      map[string]TimeSample // eg key watcher.<watcherId>.<checkId>
	changer    *ChangeHandler        // optional change handler
}

func NewStateStore(nc *nats.Conn) (*StateStore, error) {
	// the optional nats connection is for changer

	scanPeriod, _ := time.ParseDuration("10s")
	timeout, _ := time.ParseDuration("30s")
	s := StateStore{
		lock:       &sync.RWMutex{},
		store:      make(map[string]TimeSample),
		scanPeriod: scanPeriod,
		timeout:    timeout,
	}
	// add optionnal change handler
	if nc != nil {
		c, err := NewChangeHandler(nc)
		if err != nil {
			log.Printf("NewStateSore: cannot create ChangeHadler:%s\n", err.Error())
			return nil, err
		} else {
			s.changer = c
		}
	}
	return &s, nil
}

// Add : add key:status  to store
func (s *StateStore) Add(key string, status float64) {
	// sample key : watcher.watcherId.checkId or watcher.tb2.adm.adm_api.status
	s.lock.Lock()
	// get previous
	previous, ok := s.store[key]
	// create or update subject
	s.store[key] = TimeSample{time.Now(), status}
	s.lock.Unlock()

	if s.changer != nil {
		// changer present
		if ok == false {
			// no previous subject : notify creation
			s.changer.Send(key, int(status))
			return
		}
		// already exists : check if status has change
		if status != previous.Status {
			s.changer.Send(key, int(status))
		}
	} else {
		// no changer : local notify
		// changer present
		if ok == false {
			// no previous subject : notify creation
			log.Printf("StateStore.Add: create local notity : change.%s => %f\n", key, status)
			return
		}
		// already exists : check if status has change
		if status != previous.Status {
			log.Printf("StateStore.Add: update local notity : change.%s => %f\n", key, status)
		}
	}
}

// Get : get a specific subject time sample
func (s *StateStore) Get(subject string) (TimeSample, bool) {
	s.lock.RLock()
	defer s.lock.RUnlock()
	t, ok := s.store[subject]
	return t, ok

}

// GetAll : get all subjects
func (s *StateStore) GetAll() map[string]TimeSample {
	s.lock.RLock()
	defer s.lock.RUnlock()
	m := make(map[string]TimeSample)
	for k, v := range s.store {
		m[k] = v
	}
	return m
}

// GetKeys : keys of the store
func (s *StateStore) GetKeys() (keys []string) {
	s.lock.RLock()
	defer s.lock.RUnlock()
	for k := range s.store {
		keys = append(keys, k)
	}
	// sort keys
	sort.Strings(keys)
	return keys
}

// Run : starts the check loop until ctx.Done
func (s *StateStore) Run(ctx context.Context) {

	//go func() {

	var subjects []string
	for {
		// check Done
		timer := time.NewTimer(s.scanPeriod)
		select {
		case <-ctx.Done():
			return
		case <-timer.C:
			//pass
		}

		// check loop
		//log.Printf("IdleCheck: start scanning subjects")
		now := time.Now()
		//slock.RLock()

		// collect all keys
		subjects = nil
		s.lock.RLock()
		for subject, _ := range s.store {
			subjects = append(subjects, subject)
		}
		s.lock.RUnlock()
		if len(subjects) <= 0 {
			continue
		}

		for _, subject := range subjects {
			//for subject, ts := range s.store {
			s.lock.RLock()
			ts, ok := s.store[subject]
			s.lock.RUnlock()
			if ok {
				if ts.Status == 0 {
					// reset already Done
					continue
				}
				if now.After(ts.Timestamp.Add(s.timeout)) {
					// timeout : emit a metric with value 0
					topic := watcher.WatcherSubject{}
					err := topic.Parse(subject)
					if err == nil {
						switch topic.Prefix {
						case "watcher":
							// Add a timeSample with status 0
							s.Add(subject, 0)
							// make a watcher_check_status metric with value 0
							WatcherCheckStatus.WithLabelValues(
								topic.Platform, topic.Component, topic.WatcherID, topic.CheckID).Set(IdleValue)
						}

					}
					//tb := strings.Split(subject, ".")
					//if len(tb) >= 3 {
					//	// split subject
					//	prefix := tb[0]  // eg watcher
					//	itemID := tb[1]  // eg watcherId
					//	checkID := tb[2] // eg checkId
					//
					//	// reset the timeSample
					//	//s.store[subject] = TimeSample{now, 0}
					//
					//	switch prefix {
					//	case "watcher":
					//		// Add a timeSample with status 0
					//		s.Add(fmt.Sprintf("%s.%s.%s", prefix, itemID, checkID), 0)
					//		// make a watcher_check_status metric with value 0
					//		WatcherCheckStatus.WithLabelValues(itemID, checkID).Set(IdleValue)
					//	}
					//}
				}
			} // ok
		}
		//slock.RUnlock()
		//log.Printf("IdleCheck: stop scanning subjects (%s)\n", time.Since(now))

	}
	//}()
}
