package collector

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"strings"
	"time"

	"bitbucket.org/cocoon/trainmvp/documentor"

	"bitbucket.org/cocoon/trainmvp/watcher"
	"github.com/nats-io/nats.go"
	"github.com/prometheus/client_golang/prometheus"
)

var (
	WatcherCheckStatus = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "watcher_check_result",
			Help: "status of a watcher check 1=OK , 0=KO",
		},
		[]string{"platform", "component", "watcher", "check"},
	)

	ComponentCheckStatus = prometheus.NewGaugeVec(
		prometheus.GaugeOpts{
			Name: "component_check_status",
			Help: "status of a component check 1=OK , 0=KO",
		},
		[]string{"component"},
	)

	Heartbeat = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "Heartbeat",
			Help: "heartbeat count",
		},
		[]string{"category", "name"},
	)

	WatcherCheckCount = prometheus.NewCounterVec(
		prometheus.CounterOpts{
			Name: "watcher_check_count",
			Help: "watcher check count",
		},
		[]string{"platform", "component", "watcher", "check"},
		//[]string{"watcher", "check"},
	)

	// declare global storage
	storage *StateStore

	// declare global IdleChecker
	//IdleChecker *IdleCheck
	IdleChecker *StateStore
)

// full collect server .  use InitCollector instead
func Collect(ctx context.Context, nc *nats.Conn, addr string) {

	if addr == "" {
		addr = ":8080"
	}

	// register watcher prometheus metrics
	prometheus.MustRegister(WatcherCheckStatus)
	prometheus.MustRegister(WatcherCheckCount)
	prometheus.MustRegister(ComponentCheckStatus)
	prometheus.MustRegister(Heartbeat)

	// create global IdleChecker
	//timeout, err := time.ParseDuration("30s")
	//if err != nil {
	//	log.Printf("Warning: invalid idle timeout: assume 30s")
	//	timeout = 30 * time.Second
	//}
	// Launch idleChecker ( send message status =0 if no message received from a watcher )
	//IdleChecker = NewIdleCheck(timeout)
	//IdleChecker.Run(ctx)
	IdleChecker, _ = NewStateStore(nc)
	go IdleChecker.Run(ctx)

	// IdleCheckerService : get.collector.store.all() get.collector.store.key(string)
	IdleCheckerService := NewIdleCheckService(nc, IdleChecker)
	IdleCheckerService.Run(ctx)

	// create Documentor service
	doc := documentor.NewDocumentor(nc, "Documentation.watchers.>")
	doc.Run(ctx)

	// start server
	srv := StartHttpServer(addr)

	// subscribe to watcher.>
	sub, err := nc.Subscribe("watcher.>", MakeWatcherMetric)
	if err != nil {
		fmt.Printf("failed to subscribe to watcher.>\n")
		return
	}

	// subscribe to component.>
	sub2, err := nc.Subscribe("component.>", MakeComponentMetric)
	if err != nil {
		fmt.Printf("failed to subscribe to component.>\n")
		return
	}

	// subscribe to Heartbeat.>
	sub3, err := nc.Subscribe("Heartbeat.>", MakeHeartbeatMetric)
	if err != nil {
		fmt.Printf("failed to subscribe to Heartbreat.>\n")
		return
	}

	go func() {
		select {
		case <-ctx.Done():
			// shut down gracefully, but wait no longer than 5 seconds before halting
			sub.Unsubscribe()
			sub2.Unsubscribe()
			sub3.Unsubscribe()

			ctx2, _ := context.WithTimeout(context.Background(), 5*time.Second)
			srv.Shutdown(ctx2)
			log.Println("collector server gracefully stopped")
		}
	}()

}

func InitCollector(ctx context.Context, nc *nats.Conn, timeoutString string) {

	// register watcher prometheus metrics
	prometheus.MustRegister(WatcherCheckStatus)
	prometheus.MustRegister(WatcherCheckCount)
	prometheus.MustRegister(ComponentCheckStatus)
	prometheus.MustRegister(Heartbeat)

	// create a status store and make it global
	store, err := NewStateStore(nc)
	if err == nil {
		// update global Storage
		storage = store
	}

	// create global IdleChecker
	timeout, err := time.ParseDuration(timeoutString)
	if err != nil {
		log.Printf("InitCollector: Warning: invalid idle timeout: assume 30s")
		timeout = 30 * time.Second
	}
	// idlechecker
	IdleChecker, _ = NewStateStore(nc)
	IdleChecker.timeout = timeout
	go IdleChecker.Run(ctx)

	// IdleCheckerService : get.collector.store.all() get.collector.store.key(string)
	IdleCheckerService := NewIdleCheckService(nc, IdleChecker)
	IdleCheckerService.Run(ctx)

	// subscribe to watcher.>
	sub, err := nc.Subscribe("watcher.>", MakeWatcherMetric)
	if err != nil {
		log.Printf("InitCollector: failed to subscribe to watcher.>\n")
		return
	}

	// subscribe to component.>
	sub2, err := nc.Subscribe("component.>", MakeComponentMetric)
	if err != nil {
		log.Printf("InitCollector: failed to subscribe to component.>\n")
		return
	}

	// subscribe to Heartbeat.>
	sub3, err := nc.Subscribe("Heartbeat.>", MakeHeartbeatMetric)
	if err != nil {
		log.Printf("InitCollector: failed to subscribe to Heartbreat.>\n")
		return
	}

	go func() {
		select {
		case <-ctx.Done():
			// unsubscribe
			sub.Unsubscribe()
			sub2.Unsubscribe()
			sub3.Unsubscribe()
		}
	}()

}

// MakeWatcherMetric transform watcher message to a prometheus metric
func MakeWatcherMetric(msg *nats.Msg) {

	// message subject  watcher.<watcherID>.<CheckID>
	subject := &watcher.WatcherSubject{
		"watcher", "platform", "component",
		"watcherID", "checkID"}
	//watcherID := "watcherID"
	//checkID := "CheckID"
	result := 1.0
	subject.Parse(msg.Subject)

	//tb := strings.Split(msg.Subject, ".")
	//if len(tb) >= 3 {
	//	// message  { Result : 1 }
	//	watcherID = tb[1]
	//	checkID = tb[2]
	//}
	// message body
	r := &watcher.WatcherMessage{}
	err := json.Unmarshal(msg.Data, r)
	if err == nil {
		// conversion OK
		result = float64(r.Result)
		// update watcher_check_result
		if result == 0 {
			result = -1
		}

		// create metrics
		WatcherCheckStatus.WithLabelValues(
			subject.Platform, subject.Component, subject.WatcherID, subject.CheckID).Set(result)
		// add message subject to IdleChecker
		IdleChecker.Add(msg.Subject, result)
		// update watcher_check_count
		if result != 0 {
			// increment counter
			WatcherCheckCount.WithLabelValues(subject.Platform, subject.Component, subject.WatcherID, subject.CheckID).Inc()
		}
	}
}

// MakeComponentMetric transform watcher message to a prometheus metric
func MakeComponentMetric(msg *nats.Msg) {

	// message subject  watcher.<watcherID>.<CheckID>
	componentID := "componentID"
	result := 1.0
	tb := strings.Split(msg.Subject, ".")
	if len(tb) >= 2 {
		// message  { Result : 1 }
		componentID = tb[1]
	}
	// message body
	r := &watcher.WatcherMessage{}
	err := json.Unmarshal(msg.Data, r)
	if err == nil {
		// conversion OK
		result = float64(r.Result)
		ComponentCheckStatus.WithLabelValues(componentID).Set(result)
	}
}

// MakeHeartbeatMetric transform watcher message to a prometheus metric
func MakeHeartbeatMetric(msg *nats.Msg) {

	// message subject  watcher.<watcherID>.<CheckID>
	category := "category"
	name := "name"

	tb := strings.Split(msg.Subject, ".")
	if len(tb) >= 3 {
		// message  { Result : 1 }
		category = tb[1]
		name = tb[2]
	}

	Heartbeat.WithLabelValues(category, name).Inc()

}
