package collector

import (
	"fmt"
	"io"
	"log"
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

var version = "0.1"

func StartHttpServer(addr string) *http.Server {

	srv := &http.Server{Addr: addr}

	// home page
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		text := fmt.Sprintf("Collector version:%s", version)
		io.WriteString(w, text)
	})

	// Expose the registered metrics via HTTP.
	http.Handle("/metrics", promhttp.Handler())

	log.Printf("Httpserver: ListenAndServe() addr: %s", addr)
	go func() {
		if err := srv.ListenAndServe(); err != nil {
			// cannot panic, because this probably is an intentional close
			log.Printf("Httpserver: ListenAndServe() error: %s", err)
		}
	}()

	// returning reference so caller can call Shutdown()
	return srv
}
