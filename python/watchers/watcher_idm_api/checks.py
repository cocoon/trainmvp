# checks.py
import requests

import logging
log = logging.getLogger()

def status():
    """
        IDM API test status : return True if OK , False if not
    """
    # IDM_API_STATUS
    url = 'http://10.170.208.166:8090/api/v1/status'
    headers = {'user-agent': 'my-app/0.0.1'}
    params = (
        ('p1', 'parameter_1'),
       )
    try :
        response = requests.get(
            url,
            params=params,
            headers=headers,
            )
    except :
        print("check_one : cannot connect\n")
        return False

    result = False
    if response.status_code == 200 :

        # analyse text and set result
        response_text = response.text
        print(response.text)
        #response_text = response.json()
        result = True
    return result


if __name__ == "__main__":

    log.setLevel(logging.DEBUG)

    log.info("watcher.adm_api.status -> %s\n" %  status())
    log.info("Done...")
