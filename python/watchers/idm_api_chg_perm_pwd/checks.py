# checks.py
#import requests
import subprocess

import logging
log = logging.getLogger()

def check_00_CLEAN_START():
    """
        IDM API  clean start: 

        return True if OK , False if not
    """
    result = False
    log.debug("run 00_CLEAN_START.sh")
    process = subprocess.run(['sh', './00_CLEAN_START.sh','variables.properties_TB2'], check=False, stdout=subprocess.PIPE, universal_newlines=True)
    output = process.stdout
    log.debug("%s\n",process.stdout)

    if process.returncode == 0 :
        result = True

    return result







if __name__ == "__main__":

    log.setLevel(logging.DEBUG)

    log.info("watcher.adm_api_chg_perm_pwd.clean_start -> %s\n" %  check_00_CLEAN_START())
    log.info("Done...")
