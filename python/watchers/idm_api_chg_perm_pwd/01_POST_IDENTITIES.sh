#!/bin/bash
unset https_proxy
unset http_proxy
source $1
#OBTENTION BEARER
GATE_BEARER_identities_CDM=`curl -k -i -s -X POST -u "${okapi_user}:${okapi_pwd}" -H "accept: application/json" -H "Content-Type: application/x-www-form-urlencoded" -d "grant_type=client_credentials&scope=${okapi_liste_scope}"  https://okapi-v2.hbx.geo.francetelecom.fr/v2/token | grep access_token | awk -F'"' '{print $4;}'`

#POST IDENTITY WITH IDM API
curl -k -s -X POST -H 'Content-Type: application/json;charset=utf-8' -H "X-IDM-Origin-Id: 44F" -H "X-Client-Id: FCP" -H "Authorization: Bearer ${GATE_BEARER_identities_CDM}" -d '{
  "identifiers": [ { "value": "'${LOGIN_A_CREER}'", "provider": "ORANGE", "type": "LOGIN" } ], 
  "passwords":   [ { "value": "temp1", "type": "temporary" } ], 
  "profile": { "civility": "3", "firstName": "Gaetan", "lastName": "Gogail", "displayName": "Gaetan Gogail", "birthdate": "1996-09-04", "language": "fr-FR", "timeZone": "Europe/Paris" }
}' "${idm_api_url}/api/v1/identities"

#GET ULV WITH WASSUP
X_WASSUP_ULV=`curl -s -H "X_WASSUP_LOGIN: ${LOGIN_A_CREER}" "${wassup_url}/?wassup=ident&info=ulv,guc&serv=${wassup_service}&prot=200&cvt=4" | grep X_WASSUP_ULV | cut -d"=" -f2`

if [ -z ${X_WASSUP_ULV} ]; then
	echo echec
	echo "X_WASSUP_ULV=${X_WASSUP_ULV}"
	exit 1
else
	echo succes
	echo "X_WASSUP_ULV=${X_WASSUP_ULV}"
	exit 0
fi


