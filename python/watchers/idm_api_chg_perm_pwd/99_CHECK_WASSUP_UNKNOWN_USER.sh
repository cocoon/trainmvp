#!/bin/bash
unset https_proxy
unset http_proxy
source $1
#GET ULV WITH WASSUP

curl -s -H "X_WASSUP_LOGIN: ${LOGIN_A_CREER}" "${wassup_url}/?wassup=ident&info=ulv&serv=${wassup_service}&prot=200&cvt=4"

X_WASSUP_ULV=`curl -s -H "X_WASSUP_LOGIN: ${LOGIN_A_CREER}" "${wassup_url}/?wassup=ident&info=ulv,guc&serv=${wassup_service}&prot=200&cvt=4" | grep X_WASSUP_ULV | cut -d"=" -f2`

if [ -z ${X_WASSUP_ULV} ]; then
        echo succes
        exit 0
else
        echo echec
        exit 1
fi


