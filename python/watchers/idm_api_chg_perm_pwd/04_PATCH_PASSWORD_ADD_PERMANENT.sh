#!/bin/bash
unset https_proxy
unset http_proxy
source $1
#OBTENTION BEARER
GATE_BEARER_identities_CDM=`curl -s -k -i -X POST -u "${okapi_user}:${okapi_pwd}" -H "accept: application/json" -H "Content-Type: application/x-www-form-urlencoded" -d "grant_type=client_credentials&scope=${okapi_liste_scope}"  https://okapi-v2.hbx.geo.francetelecom.fr/v2/token | grep access_token | awk -F'"' '{print $4;}'`

#GET ULV
X_WASSUP_ULV=`curl -s -H "X_WASSUP_LOGIN: ${LOGIN_A_CREER}" "${wassup_url}/?wassup=ident&info=ulv,guc&serv=${wassup_service}&prot=200&cvt=4" | grep X_WASSUP_ULV | cut -d"=" -f2`

#PATCH PROFILE
RETURN_CODE_PATCH_PROFILE=`curl -s -k -i -X PUT -H 'Content-Type: application/json;charset=utf-8' -H "X-IDM-Origin-Id: 44F" -H "X-Client-Id: FCP" -H "Authorization: Bearer ${GATE_BEARER_identities_CDM}" -d '{ "value": "perm1"}' "${idm_api_url}/api/v1/identities/${X_WASSUP_ULV}/passwords/permanent" | grep 'HTTP'  | tail -n1 | cut -d ' ' -f2`
if [ ${RETURN_CODE_PATCH_PROFILE} -ne 201 ]; then
	echo echec
	exit 1
else
	echo succes
	exit 0
fi


