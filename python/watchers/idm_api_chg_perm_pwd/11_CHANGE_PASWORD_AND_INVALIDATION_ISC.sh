#!/bin/bash
unset https_proxy
unset http_proxy
source $1
#GET ULV WITH WASSUP

DATE_NOW_MOINS_10_S=`date --date="-10 sec" +%Y%m%d%H%M%S`


X_WASSUP_ULV=`curl -s -H "X_WASSUP_PWD: perm1" -H "X_WASSUP_LOGIN: ${LOGIN_A_CREER}" "${wassup_url}/?wassup=ident&info=ulv,guc&serv=${wassup_service}&prot=200&cvt=4" | grep X_WASSUP_ULV | cut -d"=" -f2`

echo "AUTHENTIFICATION WASSUP, OBBTENTION SESSION ID -- SID"
echo $wassup_service_cooses
X_WASSUP_COOKIE=`curl -s -H "X_WASSUP_PWD: perm1" -H "X_WASSUP_LOGIN: ${LOGIN_A_CREER}" "${wassup_url}/?wassup=ident&info=cooses,ulv,guc&serv=${wassup_service_cooses}&prot=200&cvt=4" | grep X_WASSUP_COOSES | cut -d"=" -f2 | cut -d ";" -f1`
echo "X_WASSUP_COOKIE:  $X_WASSUP_COOKIE"
X_WASSUP_SID=`curl -s -H "X_WASSUP_COOKIE: wassup=${X_WASSUP_COOKIE};" "${wassup_url}/?wassup=ident&info=cooses,sid,ulv,guc&serv=${wassup_service_sid}&prot=200&cvt=4" | grep X_WASSUP_SID | cut -d"=" -f2 `
echo "X_WASSUP_SID:  $X_WASSUP_SID"

SESSION_ISC_VALIDE=`curl -k -H "Authorization: $arc_auth" -s "${arc_uri}/managesessions/CheckSessionIsValid?mco=OFR&wad=${DATE_NOW_MOINS_10_S}&ulv=${X_WASSUP_ULV}" | grep '<ps_code>0</ps_code>'`
echo "SESSION_ISC_VALIDE: $SESSION_ISC_VALIDE"

SESSION_TROUVEE=`curl -s -k -X GET -H 'accept: application/json' "https://dev_lannion:lannion_pwd@api-wtsmtb1privfr.itn.ftgroup:443/v1/sessions/${X_WASSUP_SID}" | grep $X_WASSUP_ULV`
echo $SESSION_TROUVEE
echo

echo "CHANGEMENT DE MOT DE PASSE PERMANENT, LE SESSION EST INVALIDEE DANS SESSION MANAGER ET DANS ISC"
GATE_BEARER_identities_CDM=`curl -s -k -i -X POST -u "${okapi_user}:${okapi_pwd}" -H "accept: application/json" -H "Content-Type: application/x-www-form-urlencoded" -d "grant_type=client_credentials&scope=${okapi_liste_scope}"  https://okapi-v2.hbx.geo.francetelecom.fr/v2/token | grep access_token | awk -F'"' '{print $4;}'`
RETURN_CODE_PATCH_PROFILE=`curl -s -k -i -X PUT -H 'Content-Type: application/json;charset=utf-8' -H "X-IDM-Origin-Id: 44F" -H "X-Client-Id: FCP" -H "Authorization: Bearer ${GATE_BEARER_identities_CDM}" -d '{ "value": "perm2"}' "${idm_api_url}/api/v1/identities/${X_WASSUP_ULV}/passwords/permanent" | grep 'HTTP'  | tail -n1 | cut -d ' ' -f2`
echo "CHANGE PASSWORD: $RETURN_CODE_PATCH_PROFILE"
SESSION_ISC_INVALIDE=`curl -k -H "Authorization: $arc_auth" -s "${arc_uri}/managesessions/CheckSessionIsValid?mco=OFR&wad=${DATE_NOW_MOINS_10_S}&ulv=${X_WASSUP_ULV}" | grep '<ps_code>-1010</ps_code>'`
echo "SESSION_ISC_INVALIDE: $SESSION_ISC_INVALIDE"
SESSION_NON_TROUVEE=`curl -s -k -X GET -H 'accept: application/json' "https://dev_lannion:lannion_pwd@api-wtsmtb1privfr.itn.ftgroup:443/v1/sessions/${X_WASSUP_SID}" | grep "Resource not found"`
echo $SESSION_NON_TROUVEE

INVALIDATION_ARC_COOKIE=`curl -s -H "X_WASSUP_COOKIE: wassup=${X_WASSUP_COOKIE};" "${wassup_url}/?wassup=ident&info=error,cooses,sid,ulv,guc&serv=${wassup_service_sid}&prot=200&cvt=4&force=update" | grep 'X_WASSUP_ERROR=INVALIDATION_ARC_COOKIE' `
echo "INVALIDATION_ARC_COOKIE: $INVALIDATION_ARC_COOKIE"

#  ON ATTEND 5 Secondes car l'insertion des evenements dans la BD Cassandra EVent History apres lecture dans Event Broker est asynchrone
sleep 5
EVENT_TROUVE=`curl -k -s "https://ieh-api-qf.itn.intraorange:443/event-history/v1/users/${X_WASSUP_ULV}/events/?limit=1" | grep $X_WASSUP_SID `
echo "EVENT_TROUVE: $EVENT_TROUVE"


if [ -z ${X_WASSUP_ULV} ] || [ -z ${X_WASSUP_SID} ] || [ -z ${X_WASSUP_COOKIE} ]  || [ -z ${SESSION_ISC_INVALIDE} ]  || [ -z ${SESSION_ISC_VALIDE} ] || [ ${RETURN_CODE_PATCH_PROFILE} -ne 200 ] ; then
        echo echec1
        exit 1
else
        echo succes1
fi

if [ -z "${SESSION_TROUVEE}" ]  || [ -z "${SESSION_NON_TROUVEE}" ]  || [ -z "${INVALIDATION_ARC_COOKIE}" ]    || [ -z "${EVENT_TROUVE}" ] ; then
        echo echec2
        exit 1
else
        echo succes2
        exit 0
fi

