# watchers.py

import checks
from pynats import NATSClient
from time  import sleep

import logging
log = logging.getLogger()

# parameters
nats_url = "nats://127.0.0.1:4222"

# configuration
watcher_name = "sm_api"
documentation = {
    "title" : {
        "Version" : "0.1",
        "Text": "watcher sm api",
    },
    "status" : {
        "Version" : "0.1",
        "Text": "check sm api at /api/v1/status",
    }
}

# variables
watcher_prefix = "watcher.%s" % watcher_name
result_ok = b'{"Result":1}'
result_ko = b'{"Result":0}'

def send_documentation( client ) :
    """ send all watcher documentation to Documentation.<watcherId>.<checkId> """
    for k,v in documentation.items() :
        subject = "Documentation.watchers.%s.%s" , watcher_name, k
        # send documentation  eg: Documentation.sm_api.status
        client.publish(subject,v)

def send_message( client, check_name, result) :
    """ watcher.<watcherId>.<checkId> """
    subject = "%s.%s" % (watcher_prefix,check_name)
    client.publish( subject, payload=result)
    print("%s -> %s" % (subject, result))

def loop( nats_url) : 
    """
        main watcher loop
    """

    with NATSClient(nats_url) as client:
        send_documentation(client)
        while True:
            #
            # check status
            #
            check_name = "status"
            if checks.status() == True:
                send_message( client , check_name, result_ok)
            else :
                send_message( client , check_name, result_ko)

            sleep(5)

if __name__ == "__main__" :

    log.setLevel(logging.DEBUG)
    log.info("starting watcher: %s\n" % watcher_name )

    nats_url = "nats://127.0.0.1:4222"
    loop(nats_url)

