# checks.py
import requests

import logging
log = logging.getLogger()

def status():
    """
        EH API test status : return True if OK , False if not
    """
    # EH_API_STATUS
    url = 'https://ieh-api-qf.itn.intraorange:443/event-history/v1/status'
    headers = {'user-agent': 'my-app/0.0.1'}
    params = (
        ('p1', 'parameter_1'),
       )
    try :
        response = requests.get(
            url,
            params=params,
            headers=headers,
            )
    except :
        print("check_one : cannot connect\n")
        return False

    result = False
    if response.status_code == 200 :

        # analyse text and set result
        response_text = response.text
        print(response.text)
        #response_text = response.json()
        result = True
    return result


if __name__ == "__main__":

    log.setLevel(logging.DEBUG)

    log.info("watcher.eh_api.status -> %s\n" %  status())
    log.info("Done...")
