package watcher_test

import (
	"bitbucket.org/cocoon/trainmvp/watcher"
	"context"
	"fmt"
	"github.com/nats-io/nats.go"
	"testing"
	"time"
)

func TestWatcher(t *testing.T) {

	// Connect to a server
	nc, err := nats.Connect("127.0.0.1:4222")
	if err != nil {
		fmt.Printf("%s\n", err)
		t.Fail()
		return
	}

	ctx, cancel := context.WithCancel(context.Background())

	watcher.SampleRun(ctx, nc)

	time.Sleep(5 * time.Second)

	cancel()
	fmt.Println("Done")

}
