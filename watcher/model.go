package watcher

import (
	"github.com/pkg/errors"
	"strings"
)

// WatcherMessage
type WatcherMessage struct {
	Result int
}

// WatcherSubject   eg watcher.tb2.adm.adm_api.status
type WatcherSubject struct {
	Prefix    string // watcher
	Platform  string // tb2 / tb1
	Component string //  adm eh
	WatcherID string // adm_api
	CheckID   string // status
}

// Subject  return subject prefix.platform.component.watcher.check or prefix.watcher.check
func (w *WatcherSubject) Subject() string {
	var s []string
	s = append(s, w.Prefix)
	if w.Platform != "" {
		s = append(s, w.Platform)
	}
	if w.Component != "" {
		s = append(s, w.Component)
	}
	s = append(s, w.WatcherID)
	s = append(s, w.CheckID)
	subject := strings.Join(s, ".")
	return subject
}
func (w *WatcherSubject) Parse(subject string) error {
	tb := strings.Split(subject, ".")
	switch len(tb) {
	case 3: // old format watcher.watcherID.checkID
		w.Prefix = tb[0]
		w.Platform = "platform"
		w.Component = "component"
		w.WatcherID = tb[1]
		w.CheckID = tb[2]
		return nil
	case 5: // new format watcher.platform.component.watcherID.checkID
		w.Prefix = tb[0]
		w.Platform = tb[1]
		w.Component = tb[2]
		w.WatcherID = tb[3]
		w.CheckID = tb[4]
		return nil
	default: // bad format
		w.Prefix = "unknown"
		w.Platform = "unknown"
		w.Component = "unknown"
		w.WatcherID = "unknown"
		w.Component = "unknown"
		return errors.New("bad subject")
	}

}

type Watcher struct {
	WatcherID string
}
