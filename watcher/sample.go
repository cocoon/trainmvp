package watcher

import (
	"context"
	"fmt"
	nats "github.com/nats-io/nats.go"
	"time"
)

// a fake watcher for test

func SampleRun(ctx context.Context, nc *nats.Conn) {

	c, _ := nats.NewEncodedConn(nc, nats.JSON_ENCODER)
	defer c.Close()

	msg := &WatcherMessage{Result: 1}

	go func() {
		for {

			select {
			case <-ctx.Done():
				return

			default:
				c.Publish("watcher.Sample.Check", msg)
				fmt.Printf("watcher publish on watcher.Sample.Check\n")
				time.Sleep(1 * time.Second)
			}

		}
	}()

}
