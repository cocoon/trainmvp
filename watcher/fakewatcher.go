package watcher

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"math/rand"
	"time"

	trainmvp "bitbucket.org/cocoon/trainmvp/pkg"
	"github.com/nats-io/nats.go"
)

/*
	a fake watcher publishing  available status each <Period>  of time

	5 percent chance ( <Rate>) to publish unvalaible status for a period of <Downtime> in sec


*/

// a fake watcher for test
type FakeWatcher struct {
	*nats.Conn
	//WatcherID string
	//CheckID   string
	Watcher  *WatcherSubject
	Period   time.Duration // period between 2 status messages  eg 1s
	Rate     int           // rate is percentage 0 - 100%
	Downtime int           // number of period the watcher will stay in bad status ,eg 3

}

type DocEntry struct {
	Version string
	Text    string
}

func NewFakeWatcher(nc *nats.Conn, watcherId, checkID string) (w *FakeWatcher) {

	// default values : a message each 10s , 5% chance to become unavailable for 3 period
	period, _ := time.ParseDuration("10s")
	rate := 5 // 10% of chance to become unavailable
	downtime := 3

	watcher := &WatcherSubject{"watcher", "", "", watcherId, checkID}
	w = &FakeWatcher{nc, watcher, period, rate, downtime}

	return w
}

func (w *FakeWatcher) Run(ctx context.Context) {

	// starts heartbeat
	//s := fmt.Sprintf("watcher.%s", w.WatcherID)
	subject := w.Watcher.Subject()
	h, _ := trainmvp.NewHeartbeat(w.Conn, subject, 1)
	h.Start(ctx)

	c, _ := nats.NewEncodedConn(w.Conn, nats.JSON_ENCODER)
	//defer c.Close()
	w.SendDoc()
	// subject : watcher.watcherID.checkID
	//subject := fmt.Sprintf("watcher.%s.%s", w.WatcherID, w.CheckID)

	rand.Seed(time.Now().Unix())

	status := 0
	available := &WatcherMessage{Result: 1}
	unavailable := &WatcherMessage{Result: 0}

	go func() {
		for {

			// check Done
			select {
			case <-ctx.Done():
				return
			default:
			}

			switch status {

			case 0:
				// Available state
				err := c.Publish(subject, available)
				if err != nil {
					log.Printf("watcher: publish error: %s\n", err.Error())
				} else {
					log.Printf("watcher %s OK\n", subject)
				}

				// compute chance to become unavailable
				rate := rand.Intn(100)
				if rate < w.Rate {
					// switch to state unavailable
					status = w.Downtime
				}

			default:
				// unavailable state 1 ... downtime
				err := c.Publish(subject, unavailable)
				if err != nil {
					log.Printf("watcher: publish error: %s\n", err.Error())
				} else {
					log.Printf("watcher %s KO !!!\n", subject)
				}
				status -= 1

			}

			// tempo
			time.Sleep(w.Period)

		}
	}()

}

func (w *FakeWatcher) SendDoc() {

	entry := DocEntry{
		Version: "0.1",
		Text:    "Documentation",
	}
	//subject := fmt.Sprintf("Documentation.watchers.%s.%s", w.WatcherID, w.CheckID)
	subject := fmt.Sprintf("Documentation.watchers.%s.%s", w.Watcher.WatcherID, w.Watcher.CheckID)
	data, err := json.Marshal(entry)
	if err == nil {
		log.Printf("fakewatcher.SendDoc: %s , %s\n", subject, data)
		w.Publish(subject, data)
	}

}
