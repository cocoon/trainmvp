
collector
=========


collector is a go process to collect watcher events and build prometheus metrics

* subscribe to nats subject channel watcher.> , Heartbeat.>
* publish prometheus metrics at :8080


# build image
    docker build --force-rm --no-cache -t local/collector .