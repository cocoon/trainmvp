
## launch infra

nats + prometheus + grafana

    docker-compose -f docker-compose.yml up

##  launch a fake watcher

    ./fakewatcher --watcher watcher1 --check check1 --period 5 --rate 2 --downtime 5 --nats nats://192.168.99.100:4222


## launch a fake component

    ./fakecomponent --nats nats://192.168.99.100:4222

## launch a subscriber

    ./subscriber --nats nats://192.168.99.100:4222


## connect to  prometheus

    firefox 192.168.99.100:9090

some metrics to check:

* Heartbeat
* watcher_check_result
* component_check_status



## connect to grafana grafana

    firefox 192.168.99.100:3000
    
    account :  admin/password
    
data source

    Name: trainmvp
    Type: prometheus

    URL: http://192.168.99.100:9090
    Acces: proxy




# build docker-compose

    docker-compose  -f docker-compose-host.yml  build --force-rm --no-cache
    docker-compose  -f docker-compose-host.yml  up
    
