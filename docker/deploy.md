# deploy train mvp 

## prerequisites

* docker
* docker-compose
* git 
* make
* golang (optional)


## build train mvp

1) clone repository if not done

    git clone https://cocoon_bitbucket@bitbucket.org/cocoon/trainmvp.git
    cd trainmvp

2) build docker images

    make build-images

note: this operation takes 5 minute or more

3) configure

for each of the followin file replace 192.168.99.100 with the vm address

    docker/docker-compose.yml
    docker/prometheus/prometheus.yml


4) build docker compose

    cd docker && docker-compose -f docker-compose.yml build --force-rm --no-cache

note: when launch for the first time this operation pull docker images 
for 
* prometheus
* alertmanager
* graffana
* nats

and can take a long time

5) build tools (optional)

will build fakewatcher / subscriber / fakecomponent under build/package/<platform>

    make build-watchers

# run it 

    cd docker && docker-compose -f docker-compose.yml up


# connect to trainmvp supervision

## prometheus

    firefox <host>:9090

## alertmanager

     firefox <host>:9093

## grafana

    firefox <host>:3000

    account :  admin/password
    
    data source

        Name: trainmvp
        Type: prometheus

        URL: http://<host>:9090
        Acces: proxy

## nats

    connect nats client to  nats://<host>:4222

## collector

    firefox <host>:8080/metrics
