package web

import (
	"context"
	"encoding/json"
	"net/http"
	"strings"
	"time"

	"github.com/labstack/echo-contrib/prometheus"

	"bitbucket.org/cocoon/trainmvp/web-collector/app/cook"
	"bitbucket.org/cocoon/trainmvp/web-collector/assets"

	echo "github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"

	collectorApi "bitbucket.org/cocoon/trainmvp/api"
	//"net/http"
	//"time"
)

var (
	Version = "0.2.3"
	//assets  = "../assets/"
)

type Info struct {
	Version string
}

type CollectorServer struct {
	*echo.Echo
	ctx    context.Context
	assets *assets.Assets
	//collector *collectorApi.API
	collector collectorApi.CollectorRequester
	StartTime time.Time
	// webSocketServer to send message to browser
	WebSocketServer *WebSocketServer

	Description string
	Version     string
	Links       map[string]string
	ApiVersion  string
}

// NewcollectorServer : create the server
func NewCollectorServer(ctx context.Context, assets *assets.Assets, collector collectorApi.CollectorRequester) *CollectorServer {

	e := echo.New()
	c := &CollectorServer{
		Echo:      e,
		ctx:       ctx,
		assets:    assets,
		collector: collector,

		Description: "Collector web app",
		Version:     Version,
		Links:       make(map[string]string),
	}

	c.Logger.SetLevel(log.DEBUG)
	c.Use(middleware.Logger())
	c.Use(middleware.Recover())

	// "assets/templates/*.tmpl"
	path := assets.StaticDir
	c.Static("/", path)

	c.Renderer = c.assets.GetEchoRenderer()

	// Enable metrics middleware
	// http://hostname:port/metrics
	p := prometheus.NewPrometheus("echo", nil)
	p.Use(e)

	c.SetRoutes()

	c.StartTime = time.Now()
	return c
}

func (c *CollectorServer) SetRoutes() {
	c.GET("/home", c.Home)
	c.GET("/dashboard", c.DashBoard)
	c.GET("/watcher/check/:filter", c.WatcherCheck) // filter : all or alert
	c.GET("/watcher/documentation", c.WatcherDocumentation)

	c.GET("/info", c.Info)
	c.GET("/api/doc", c.ApiDoc)
	c.POST("/api/ng/:app/*", c.NatsGateway)

}

func (c *CollectorServer) Home(ctx echo.Context) (err error) {

	return ctx.Render(http.StatusOK, "home.tmpl", c)
}

func (c *CollectorServer) WatcherDocumentation(ctx echo.Context) (err error) {
	// build template data for documentation
	data, err := cook.DocumentListTemplater(c.collector)
	return ctx.Render(http.StatusOK, "watcher_documentation.tmpl", data)
}

func (c *CollectorServer) WatcherCheck(ctx echo.Context) (err error) {

	// filter:  all or alert  ( default to all )
	filter := ctx.Param("filter")
	// build template data for watcher status
	data, err := cook.WatcherStatusTemplater(c.collector, filter)
	return ctx.Render(http.StatusOK, "watcher_check.tmpl", data)
}

func (c *CollectorServer) DashBoard(ctx echo.Context) (err error) {
	// build template data for dashboard
	data, err := cook.DashboardTemplater(c.collector)
	return ctx.Render(http.StatusOK, "dashboard.tmpl", data)
}

// Info an echo handler for general info
func (api *CollectorServer) Info(ctx echo.Context) error {
	apiVersion := collectorApi.Version
	return ctx.JSON(http.StatusOK, map[string]string{
		"description": api.Description,
		"name":        "collector",
		"version":     api.Version,
		"start_time":  api.StartTime.String()[:19],
		"ApiVersion":  apiVersion,
	})
}

// jsondoc  : download watcher documentation
func (api *CollectorServer) ApiDoc(ctx echo.Context) error {

	// fetch documentation list from api
	data, err := api.collector.GetWatcherDocumentation()
	if err == nil {
		return ctx.JSON(http.StatusOK, data)
	}
	return ctx.JSON(http.StatusOK, data)
}

//
//	a gateway to push watcher message to nats
//
//  /api/ng/watcher/{ptf}/{component}/{watcherID}/{checkID}
//
func (api *CollectorServer) NatsGateway(ctx echo.Context) (err error) {

	// extract targeted app
	app := ctx.Param("app")
	if app != "watcher" {
		// does not handle app != watcher
		return ctx.JSON(http.StatusOK, map[string]string{
			"status":  "KO",
			"error":   "unknown app",
			"version": api.Version,
		})
	}
	// extract subject from url ( after /api/ng )
	uri := ctx.Request().URL.Path
	parts := strings.Split(uri, "/")
	// remove trailing /
	if parts[len(parts)-1] == "" {
		parts = parts[0 : len(parts)-1]
	}
	subject := strings.Join(parts[3:], ".")

	// extract message from body
	type msg struct {
		Result int `json:"Result"`
	}
	u := new(msg)
	if err = ctx.Bind(u); err != nil {
		return
	}

	body, err := json.Marshal(u)

	// send message to nats
	sender, err := api.collector.CollectorClient()
	if err != nil {
		return
	}
	_ = sender
	sender.Publish(subject, body)
	//nc,err := api.collector.

	return ctx.JSON(http.StatusOK, map[string]string{
		"status":  "OK",
		"version": api.Version,
		"subject": subject,
		"body":    string(body),
	})
}
