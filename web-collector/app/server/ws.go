package web

import (
	"encoding/json"
	"log"
	"strings"

	"bitbucket.org/cocoon/trainmvp/watcher"
	"github.com/labstack/echo/v4"
	"github.com/nats-io/nats.go"
	"golang.org/x/net/websocket"
)

// WsMessage message passed via channel
type WsMessage struct {
	subject string
	Msg     []byte
	//Msg interface{}
}

// BrowserMessage The message send to browser via websocket
type BrowserMessage struct {
	Subject string
	Msg     watcher.WatcherMessage //  { Result int }
}

func (m *BrowserMessage) Encode() (data []byte, err error) {
	data, err = json.Marshal(m)
	return
}
func (m *BrowserMessage) Decode(data []byte) (err error) {
	//bm = BrowserMessage{}
	err = json.Unmarshal(data, &m)
	return
}
func NewBrowserMessage(subject string, msg watcher.WatcherMessage) BrowserMessage {
	mb := BrowserMessage{}
	mb.Subject = subject
	mb.Msg = msg
	return mb
}

// NewBrowserMessageFromRaw create BrowserMessage from raw watcherMessage
func NewBrowserMessageFromRaw(subject string, raw []byte) (BrowserMessage, error) {
	// raw : byte representation of WatcherMessage { Result: int}
	wm := watcher.WatcherMessage{}
	err := json.Unmarshal(raw, &wm)
	if err != nil {
		return BrowserMessage{}, err
	}
	return NewBrowserMessage(subject, wm), nil
}

type WebSocketServer struct {
	wsChannel chan WsMessage
}

func NewWebsocketServer() *WebSocketServer {
	s := &WebSocketServer{
		wsChannel: make(chan WsMessage, 10),
	}
	return s
}

// StartWebSocket : ws socket to send update status message to browser
// { subject: "watcher.watcherId.checkId" , msg: "1" }
func (ws *WebSocketServer) RegisterWebSocket(e *echo.Echo) {

	// add websocket handler for /ws
	e.GET("ws", ws.wsHandlerFactory())

}

// WsHandlerFactory a factory for wshadler
// usage :  e.GET("/ws", WshandlerFactory())
// a closure to get reference to api channel
func (ws *WebSocketServer) wsHandlerFactory() func(c echo.Context) error {

	// build ws channel
	//api.wsChannel = make(chan WsMessage)

	wsHandler := func(c echo.Context) error {
		websocket.Handler(func(wsConn *websocket.Conn) {
			defer wsConn.Close()
			for {
				// blocking read from ws channel
				msg := <-ws.wsChannel //  { Subject: Msg: { Result:int}}
				bm, err := NewBrowserMessageFromRaw(msg.subject, msg.Msg)
				if err != nil {
					c.Logger().Error(err)
					continue
				}
				jsonMsg, err := bm.Encode()
				if err != nil {
					c.Logger().Error(err)
					continue
				}
				// Write json message to websocket
				err = websocket.Message.Send(wsConn, string(jsonMsg))
				if err != nil {
					c.Logger().Error(err)
				}
			}
		}).ServeHTTP(c.Response(), c.Request())
		return nil
	}
	return wsHandler
}

// WsSend : write message to channel
// used by nats subscription to send data to channel
func (ws *WebSocketServer) WsSend(subject string, msg []byte) {
	wsMsg := WsMessage{subject: subject, Msg: msg}
	ws.wsChannel <- wsMsg
}

// WsGet: get message from  channel
// used to read message from channel
func (ws *WebSocketServer) WsGet() (msg WsMessage) {
	select {

	case msg := <-ws.wsChannel:
		return msg

	}
}

func (ws *WebSocketServer) StartWatcherStatusUpdater(nc *nats.Conn) (err error) {

	nc.Subscribe("change.watcher.*.*", func(m *nats.Msg) {

		log.Printf("received message:%s\n", m.Subject)
		parts := strings.Split(m.Subject, ".")
		if len(parts) < 4 {
			log.Printf("StartWatcherStatusUpdater subscription : Incorrect subject length\n")
			return
		}
		// remap subject
		wsSubject := parts[1] + "." + parts[2] + "." + parts[3]
		// check message is a WatcherMessage
		body := watcher.WatcherMessage{}
		err := json.Unmarshal(m.Data, &body)
		if err != nil {
			log.Printf("StartWatcherStatusUpdater subscription : %s\n", err.Error())
			return
		}
		//status := strconv.Itoa(body.Result)
		// send message to ws
		ws.WsSend(wsSubject, m.Data)
		//fmt.Printf("Received a message: %s\n", string(m.Data))
	})
	return
}
