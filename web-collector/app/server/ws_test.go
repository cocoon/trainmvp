package web

import (
	"encoding/json"
	"fmt"
	"testing"

	"bitbucket.org/cocoon/trainmvp/watcher"
)

func TestBrowserMessage(t *testing.T) {

	wm := watcher.WatcherMessage{Result: 1}

	bm1 := NewBrowserMessage("watcher.watcher1.check1", wm)
	if bm1.Msg.Result != 1 {
		t.Fail()
		return
	}
	rawWm, _ := json.Marshal(wm)
	bm2, err := NewBrowserMessageFromRaw("watcher.watcher1.check1", rawWm)
	if err != nil {
		t.Fail()
		return
	}
	if bm2.Msg.Result != 1 {
		t.Fail()
		return
	}

	jbm, err := bm1.Encode()
	if err != nil {
		t.Fail()
		return
	}
	fmt.Printf("%s\n", jbm)

	bm3 := BrowserMessage{}
	err = bm3.Decode(jbm)
	if err != nil {
		t.Fail()
		return
	}
	if bm3.Msg.Result != 1 {
		t.Fail()
		return
	}

}

func TestWebSocketServer(t *testing.T) {

	s := NewWebsocketServer()

	wm := watcher.WatcherMessage{Result: 1}
	jm, err := json.Marshal(wm)
	if err != nil {
		t.Fail()
		return
	}

	s.WsSend("watcher.wharId.checkID", jm)

	received := s.WsGet()

	_ = received

	return

}
