package collector

/*

	a link to bitbucket.org/cocoon/collector



*/

import (
	"context"

	"bitbucket.org/cocoon/trainmvp/api"
	origin "bitbucket.org/cocoon/trainmvp/collector"
	"bitbucket.org/cocoon/trainmvp/documentor"
)

// CollectorRequester : an interface to query collector services
type CollectorRequester interface {
	GetWatcherDocumentation() (documentor.WatcherDocumentation, error)
	GetWatcherStatus() (map[string]origin.TimeSample, error)
}

//type DocumentationEntry documentor.Entry

func NewCollector(ctx context.Context, urlNats string) *api.API {

	c := api.NewAPI(ctx, urlNats)

	return c
}
