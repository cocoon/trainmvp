package cook

import (
	"os"
	"testing"

	"bitbucket.org/cocoon/trainmvp/web-collector/app/mock"
	"bitbucket.org/cocoon/trainmvp/web-collector/assets"
)

func TestDashboardTemplater(t *testing.T) {

	api, _ := mock.NewCollectorRequesterApi()

	data, err := DashboardTemplater(api)
	if err != nil {
		t.Fail()
		return
	}
	_ = data

	a := assets.NewAssets("", "")
	err = a.LoadTemplate()
	if err != nil {
		t.Fail()
		return
	}

	a.RenderScreen(os.Stdout, "dashboard.tmpl", data)

}
