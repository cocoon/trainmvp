package cook

import (
	"os"
	"testing"

	"bitbucket.org/cocoon/trainmvp/web-collector/app/mock"
	"bitbucket.org/cocoon/trainmvp/web-collector/assets"
)

func TestDocumentTemplater(t *testing.T) {

	api, _ := mock.NewCollectorRequesterApi()

	data, err := DocumentListTemplater(api)
	if err != nil {
		t.Fail()
		return
	}
	_ = data

	a := assets.NewAssets("", "")
	a.LoadTemplate()

	a.RenderScreen(os.Stdout, "watcher_documentation.tmpl", data)

}
