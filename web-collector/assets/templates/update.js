
var loc = window.location;
var uri = 'ws:';

if (loc.protocol === 'https:') {
    uri = 'wss:';
}
uri += '//' + loc.host;
uri += '/ws';

//uri = "ws://192.168.1.10:1323/ws"
ws = new WebSocket(uri)

ws.onopen = function() {
    console.log('Connected to ws socket');
}
ws.onclose = function() {
    console.log('ws socket closed');
}

ws.onmessage = function(evt) {
    console.log('received ws socket message: ' + evt.data);
    // message structure :
    // { "Subject": "watcher.watcherId.checkId", "Msg": {"Result": -1} }

    var obj = JSON.parse(evt.data);
    // extract subject and msg from json messsage
    var subject = obj.Subject;
    // var msg = obj.Msg
    // convert byte array to string
    var watcherMsg = obj.Msg ;

    // split subject in kind , watcher , check
    var parts = subject.split(".", 3);
    var id = parts[1] + "." + parts[2] + ".status"; // 'watcherId.checkId.status'

    var out = document.getElementById(id);

    // set an alert
    // $('.alert').alert()

    out.innerHTML = watcherMsg.Result.toString() ;
}

//setInterval(function() {
//    ws.send('Hello, Server!');
//}, 1000);
