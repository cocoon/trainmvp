package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"time"

	"bitbucket.org/cocoon/trainmvp/api"
	web "bitbucket.org/cocoon/trainmvp/web-collector/app/server"
	assets "bitbucket.org/cocoon/trainmvp/web-collector/assets"
	"github.com/nats-io/nats-server/v2/server"
)

/*


 */

var (
	natsURL         = flag.String("nats", "nats://0.0.0.0:4222", "nats url")
	collectorURL    = flag.String("server", "0.0.0.0:8080", "web collector server")
	collectorPeriod = flag.String("period", "120s", "collector period in seconds to check idle watchers")
	watchersURL     = flag.String("watchers", "http://localhost:9001", "watcher supervisor url")
	alertManagerURL = flag.String("alertmanager", "http://localhost:9093", "alert-manager url")
	grafanaURL      = flag.String("grafana", "http://localhost:3000", "graffana url")
	prometheusURL   = flag.String("prometheus", "http://localhost:9090", "prometheus url")
	// add an optional embeded nats-server
	natsServer  = flag.Bool("nats-server", true, "embedded nats server")
	natsCluster = flag.Int("nats-cluster", 4248, "port to listen to cluster join requests")
	natsMonitor = flag.Int("nats-monitor", 8222, "port to http monitoring")
)

func main() {

	// subscribe to SIGINT signals
	stopChan := make(chan os.Signal)
	signal.Notify(stopChan, os.Interrupt)

	flag.Parse()

	ctx, cancel := context.WithCancel(context.Background())
	_ = ctx

	// do we need an embeded nats-server
	if *natsServer == true {
		// launch an embedded nats-server
		log.Printf("Launch an embeded nats-server")
		LaunchNatsServer()
	} else {
		log.Printf("will use an external nats-server")
	}

	// create a collector and start it
	c := api.NewAPI(ctx, *natsURL)
	c.Period = *collectorPeriod
	err := c.Start()
	if err != nil {
		log.Printf("%s\n", err.Error())
		return
	}
	nc, err := c.NatsConnection()
	if err != nil {
		log.Printf("%s\n", err.Error())
		return
	}

	// create web assets templater
	a := assets.NewAssets("assets", "")
	err = a.LoadTemplate()
	if err != nil {
		log.Printf("%s\n", err.Error())
		return
	}

	// create server
	s := web.NewCollectorServer(ctx, a, c)

	// add links
	s.Links["alertManager"] = *alertManagerURL
	s.Links["grafana"] = *grafanaURL
	s.Links["watchers"] = *watchersURL
	s.Links["prometheus"] = *prometheusURL

	// create web socket server
	ws := web.NewWebsocketServer()
	ws.StartWatcherStatusUpdater(nc)
	ws.RegisterWebSocket(s.Echo)

	log.Printf("web collector: starting ...")
	s.Start(*collectorURL)

	// wait for singint
	<-stopChan
	log.Println("web collector: shutting down ...")
	cancel()
	time.Sleep(2 * time.Second)

	log.Println("web collector: exited.")

}

func LaunchNatsServer() {

	opts := &server.Options{
		Host:                  "0.0.0.0",
		Port:                  4222,
		NoLog:                 true,
		NoSigs:                true,
		MaxControlLine:        4096,
		DisableShortFirstPing: true,
		HTTPPort:              *natsMonitor,
	}
	// configure monitoring
	//opts.HTTPHost = "localhost"
	//opts.HTTPPort = *natsMonitor
	// configure url to listen to cluster join request
	opts.Cluster.Host = "0.0.0.0"
	opts.Cluster.Port = *natsCluster

	// create new server
	s, err := server.NewServer(opts)
	if err != nil || s == nil {
		panic(fmt.Sprintf("No NATS Server object returned: %v", err))
	}
	// Run server in Go routine.
	go s.Start()

	// Wait for accept loop(s) to be started
	if !s.ReadyForConnections(10 * time.Second) {
		panic("Unable to start NATS Server in Go Routine")
	}
	log.Printf("Embeded nats-server started")

}
