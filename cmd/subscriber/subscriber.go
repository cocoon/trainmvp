package main

import (
	"context"
	"flag"
	"fmt"
	"github.com/nats-io/nats.go"
	"log"
	"os"
	"os/signal"
	"time"
)

var (
	natsURL = flag.String("nats", "nats://127.0.0.1:4222", "nats url")
)

/*
	a trainmvp message subscriber

	sunbscribes to

	watcher.>
	component.>
	Heartbeat.>


*/

func main() {

	// subscribe to SIGINT signals
	stopChan := make(chan os.Signal)
	signal.Notify(stopChan, os.Interrupt)

	flag.Parse()

	// create nats connection
	// Connect to a server
	nc, err := nats.Connect(*natsURL)
	if err != nil {
		fmt.Printf("%s\n", err)
		return
	}

	ctx, cancel := context.WithCancel(context.Background())

	log.Printf("subscriber: starting ...")

	_, _ = nc.Subscribe("watcher.>", func(msg *nats.Msg) {

		select {
		case <-ctx.Done():
			return

		default:
			log.Printf("subscriber: received message on %s\n", msg.Subject)
		}

	})

	_, _ = nc.Subscribe("component.>", func(msg *nats.Msg) {

		select {
		case <-ctx.Done():
			return

		default:
			log.Printf("subscriber: received message on %s\n", msg.Subject)
		}

	})

	_, _ = nc.Subscribe("Heartbeat.>", func(msg *nats.Msg) {

		select {
		case <-ctx.Done():
			return

		default:
			log.Printf("subscriber: received message on %s\n", msg.Subject)
		}

	})

	_, _ = nc.Subscribe("change.>", func(msg *nats.Msg) {

		select {
		case <-ctx.Done():
			return

		default:
			log.Printf("subscriber: received message on %s\n", msg.Subject)
		}

	})

	// wait for singint
	<-stopChan
	log.Println("subscriber: shutting down ...")
	cancel()
	time.Sleep(2 * time.Second)

	log.Println("subscriber: exited.")

}
