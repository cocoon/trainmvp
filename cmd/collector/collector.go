package main

import (
	"bitbucket.org/cocoon/trainmvp/collector"
	"context"
	"flag"
	"fmt"
	"github.com/nats-io/nats.go"
	"log"
	"os"
	"os/signal"
	"time"
)

var (
	natsURL = flag.String("nats", "nats://127.0.0.1:4222", "nats url")
	promURL = flag.String("prom", "0.0.0.0:8080", "prometheus http server")
)

func main() {

	// subscribe to SIGINT signals
	stopChan := make(chan os.Signal)
	signal.Notify(stopChan, os.Interrupt)

	flag.Parse()

	// create nats connection
	// Connect to a server
	nc, err := nats.Connect(*natsURL)
	if err != nil {
		fmt.Printf("%s\n", err)
		return
	}

	ctx, cancel := context.WithCancel(context.Background())
	collector.Collect(ctx, nc, *promURL)

	log.Printf("collector: starting ...")

	// wait for singint
	<-stopChan
	log.Println("collector: shutting down ...")
	cancel()
	time.Sleep(2 * time.Second)

	log.Println("collector: exited.")

}
