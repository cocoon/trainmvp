package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"time"

	"bitbucket.org/cocoon/trainmvp/watcher"
	"github.com/nats-io/nats.go"
)

var (
	natsURL   = flag.String("nats", "nats://127.0.0.1:4222", "nats url")
	platform  = flag.String("platform", "platform", "platform")
	component = flag.String("component", "component", "component")
	watcherID = flag.String("watcher", "watcherID", "watcher ID")
	checkID   = flag.String("check", "checkID", "check ID")
	period    = flag.Int("period", 1, "time in Second betwwen 2 messages")
	rate      = flag.Int("rate", 2, "error rate in %")
	downtime  = flag.Int("downtime", 60, "number of period the watcher stay unavailable")
)

func main() {

	// subscribe to SIGINT signals
	stopChan := make(chan os.Signal)
	signal.Notify(stopChan, os.Interrupt)

	flag.Parse()

	// create nats connection
	// Connect to a server
	nc, err := nats.Connect(*natsURL)
	if err != nil {
		fmt.Printf("%s\n", err)
		return
	}

	w := watcher.NewFakeWatcher(nc, *watcherID, *checkID)
	w.Watcher.Platform = *platform
	w.Watcher.Component = *component

	ps := fmt.Sprintf("%ds", *period)
	p, _ := time.ParseDuration(ps)
	w.Period = p
	w.Rate = *rate
	w.Downtime = *downtime

	ctx, cancel := context.WithCancel(context.Background())

	w.Run(ctx)

	log.Printf("fakewatcher: starting ...")

	// wait for singint
	<-stopChan
	log.Println("fakewatcher: shutting down ...")
	cancel()
	time.Sleep(2 * time.Second)

	log.Println("fakewatcher: exited.")

}
