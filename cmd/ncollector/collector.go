package main

/*

	ncollector : a collector with an optional embeded nats-server

	1) launch a collector with an embeded nats-server
		collector --prom 0.0.0.0:8080 --nats-server
			--nats nats://localhost:4222
			--nats-cluster nats://localhost:4248
			--nats-monitor nats://localhost:8222

		the nats-server listen to localhost:4248 to join a cluster


	2) launch a collector and connect to an existant nats server
		collector --prom 0.0.0.0:8080 --nats nats://localhost:4222


*/

import (
	"bitbucket.org/cocoon/trainmvp/collector"
	"context"
	"flag"
	"fmt"
	"github.com/nats-io/nats-server/v2/server"
	"github.com/nats-io/nats.go"
	"log"
	"os"
	"os/signal"
	"time"
)

var (
	natsServer  = flag.Bool("nats-server", false, "embeded nats server")
	natsURL     = flag.String("nats", "nats://localhost:4222", "nats url")
	natsCluster = flag.Int("nats-cluster", 4248, "port to listen to cluster join requests")
	natsMonitor = flag.Int("nats-monitor", 8222, "port to http monitoring")

	promURL = flag.String("prom", "0.0.0.0:8080", "prometheus http server")
)

func main() {

	// subscribe to SIGINT signals
	stopChan := make(chan os.Signal)
	signal.Notify(stopChan, os.Interrupt)

	flag.Parse()

	// need embeded nats-server
	if *natsServer == true {
		// launch an embeded nats-server
		log.Printf("Launch an embeded nats-server")
		LaunchNatsServer()
	} else {
		log.Printf("will use an external nats-server")
	}

	// create nats connection
	// Connect to server
	nc, err := nats.Connect(*natsURL)
	if err != nil {
		log.Printf("%s\n", err)
		return
	}

	ctx, cancel := context.WithCancel(context.Background())
	collector.Collect(ctx, nc, *promURL)

	log.Printf("collector: starting ...")

	// wait for singint
	<-stopChan
	log.Println("collector: shutting down ...")
	cancel()
	time.Sleep(2 * time.Second)

	log.Println("collector: exited.")

}

func LaunchNatsServer() {

	opts := &server.Options{
		Host:                  "localhost",
		Port:                  4222,
		NoLog:                 true,
		NoSigs:                true,
		MaxControlLine:        4096,
		DisableShortFirstPing: true,
	}
	// configure monitoring
	opts.HTTPHost = "localhost"
	opts.HTTPPort = *natsMonitor
	// configure url to listen to cluster join request
	opts.Cluster.Host = "localhost"
	opts.Cluster.Port = *natsCluster

	// create new server
	s, err := server.NewServer(opts)
	if err != nil || s == nil {
		panic(fmt.Sprintf("No NATS Server object returned: %v", err))
	}
	// Run server in Go routine.
	go s.Start()

	// Wait for accept loop(s) to be started
	if !s.ReadyForConnections(10 * time.Second) {
		panic("Unable to start NATS Server in Go Routine")
	}
	log.Printf("Embeded nats-server started")

}
