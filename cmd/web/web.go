package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"time"

	web "bitbucket.org/cocoon/trainmvp/web.old"
	"github.com/nats-io/nats.go"
)

var (
	natsURL = flag.String("nats", "nats://127.0.0.1:4222", "nats url")
	promURL = flag.String("prom", "0.0.0.0:8080", "prometheus http server")
)

func main() {

	// subscribe to SIGINT signals
	stopChan := make(chan os.Signal)
	signal.Notify(stopChan, os.Interrupt)

	flag.Parse()

	// create nats connection
	// Connect to a server
	nc, err := nats.Connect(*natsURL)
	if err != nil {
		fmt.Printf("%s\n", err)
		return
	}
	_ = nc

	ctx, cancel := context.WithCancel(context.Background())
	_ = ctx
	web.Run(ctx, *natsURL, "./assets")

	log.Printf("collector: starting ...")

	// wait for sigint
	<-stopChan
	log.Println("collector: shutting down ...")
	cancel()
	time.Sleep(2 * time.Second)

	log.Println("collector: exited.")

}
