package main

import (
	"bitbucket.org/cocoon/trainmvp/component"
	"context"
	"flag"
	"fmt"
	"github.com/nats-io/nats.go"
	"log"
	"os"
	"os/signal"
	"time"
)

var (
	natsURL   = flag.String("nats", "nats://127.0.0.1:4222", "nats url")
	watcherID = flag.String("name", "componentID", "component ID")

	period   = flag.Int("period", 10, "time in Second between 2 messages")
	rate     = flag.Int("rate", 5, "error rate in %")
	downtime = flag.Int("downtime", 3, "number of period the watcher stay unavailable")
)

func main() {

	// subscribe to SIGINT signals
	stopChan := make(chan os.Signal)
	signal.Notify(stopChan, os.Interrupt)

	flag.Parse()

	// create nats connection
	// Connect to a server
	nc, err := nats.Connect(*natsURL)
	if err != nil {
		fmt.Printf("%s\n", err)
		return
	}

	w := component.NewFakeWatcher(nc, *watcherID)

	ps := fmt.Sprintf("%ds", *period)
	p, _ := time.ParseDuration(ps)
	w.Period = p
	w.Rate = *rate
	w.Downtime = *downtime

	ctx, cancel := context.WithCancel(context.Background())

	w.Run(ctx)

	log.Printf("fakecomponent: starting ...")

	// wait for singint
	<-stopChan
	log.Println("fakecomponent: shutting down ...")
	cancel()
	time.Sleep(2 * time.Second)

	log.Println("fakecomponent: exited.")

}
