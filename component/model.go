package component

// ComponentMessage
type ComponentMessage struct {
	Result int
}

type Component struct {
	ComponentID string
}
