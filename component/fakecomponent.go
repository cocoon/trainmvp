package component

import (
	trainmvp "bitbucket.org/cocoon/trainmvp/pkg"
	"context"
	"fmt"
	"github.com/nats-io/nats.go"
	"log"
	"math/rand"
	"time"
)

/*
	a fake component publishing  available status each <Period>  of time

	Heartbeat.component.<ID>

	component.<ID>.started {}
	component.<ID>.stopped {}
	component.<ID>.msg {}


	5 percent chance ( <Rate>) to publish unvalaible status for a period of <Downtime> in sec


*/

// a fake watcher for test
type FakeComponent struct {
	*nats.Conn
	ID       string
	Period   time.Duration // period between 2 status messages  eg 1s
	Rate     int           // rate is percentage 0 - 100%
	Downtime int           // number of period the watcher will stay in bad status ,eg 3

}

func NewFakeWatcher(nc *nats.Conn, componentId string) (w *FakeComponent) {

	// default values : a message each 10s , 5% chance to become unavailable for 3 period
	period, _ := time.ParseDuration("10s")
	rate := 5 // 10% of chance to become unavailable
	downtime := 3

	w = &FakeComponent{nc, componentId, period, rate, downtime}

	return w
}

func (w *FakeComponent) Run(ctx context.Context) {

	// starts heartbeat
	s := fmt.Sprintf("component.%s", w.ID)
	h, _ := trainmvp.NewHeartbeat(w.Conn, s, 1)
	h.Start(ctx)

	// publish start message
	err := w.Publish(fmt.Sprintf("component.%s.started", w.ID), nil)
	if err != nil {
		log.Printf("component: publish error: %s\n", err.Error())
	} else {
		log.Printf("component %s STARTED\n", w.ID)
	}

	c, _ := nats.NewEncodedConn(w.Conn, nats.JSON_ENCODER)
	//defer c.Close()

	// subject : watcher.watcherID.checkID
	subject := fmt.Sprintf("component.%s.msg", w.ID)

	rand.Seed(time.Now().Unix())

	status := 0
	available := &ComponentMessage{Result: 1}
	unavailable := &ComponentMessage{Result: 0}

	go func() {
		for {

			// check Done
			select {
			case <-ctx.Done():
				// publish stopped message
				err := w.Publish(fmt.Sprintf("component.%s.stopped", w.ID), nil)
				if err != nil {
					log.Printf("component: publish error: %s\n", err.Error())
				} else {
					log.Printf("component %s STOPPED\n", w.ID)
				}
				return
			default:
			}

			switch status {

			case 0:
				// Available state
				err := c.Publish(subject, available)
				if err != nil {
					log.Printf("component: publish error: %s\n", err.Error())
				} else {
					log.Printf("component %s OK\n", subject)
				}

				// compute chance to become unavailable
				rate := rand.Intn(100)
				if rate < w.Rate {
					// switch to state unavailable
					status = w.Downtime
				}

			default:
				// unavailable state
				err := c.Publish(subject, unavailable)
				if err != nil {
					log.Printf("component: publish error: %s\n", err.Error())
				} else {
					log.Printf("component %s KO !!!\n", subject)
				}
				status -= 1

			}

			// tempo
			time.Sleep(w.Period)

		}
	}()

}
