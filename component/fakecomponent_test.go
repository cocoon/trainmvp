package component

import (
	"bitbucket.org/cocoon/trainmvp/watcher"
	"context"
	"fmt"
	"github.com/nats-io/nats.go"
	"testing"
	"time"
)

func TestFakeWatcher(t *testing.T) {

	// Connect to a server
	nc, err := nats.Connect("127.0.0.1:4222")
	if err != nil {
		fmt.Printf("%s\n", err)
		t.Fail()
		return
	}

	w := watcher.NewFakeWatcher(nc, "watcherID", "checkID")
	w.Period, _ = time.ParseDuration("1s")
	w.Rate = 10
	w.Downtime = 3

	ctx, cancel := context.WithCancel(context.Background())

	w.Run(ctx)

	time.Sleep(60 * time.Second)

	cancel()
	time.Sleep(1 * time.Second)
	fmt.Println("Done")

}
